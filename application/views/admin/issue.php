<!--css Tabel -->

<!-- Tabel -->
<div class="col-md-12 col-sm-12 col-xs-12">
    <h4 class="text-center">Data Issue</h4>
    <hr>
</div>
<div class="table-responsive">
    <p>
        <a style="margin-left: 1%" href="<?php echo site_url('Issue/create_issue')?>"><button class="btn btn-primary"><i class="fa fa-plus"></i> Input Data </button></a>
    </p>

    <?php $this->view('messages') ?>

    <!-- Filter Data -->
    <table> 
        <tr>
            <tr>
            <td><h6 style="margin-left:12%" for="filter">Filter By Project</h6></td>
            <td><h6 style="margin-left:16%" for="filter">Filter By Level</h6></td>
            <td><h6 style="margin-left:16%" for="filter">Filter By Status</h6></td>
             <td><h6 style="margin-left:16%" for="filter">Filter By Category</h6></td>

            </tr>
            <th class="filterhead" style="display:none"></th>
            <th class="filterhead"></th><th class="filterhead" style="display:none"></th>
            <th class="filterhead" style="display:none"></th><th class="filterhead" style="display:none"></th>
            <th class="filterhead" style="display:none"></th><th class="filterhead" style="display:none"></th>
            <th class="filterhead"></th><th class="filterhead"></th>
            <th class="filterhead"></th>

            
        </tr>
    </table>
    <!-- End Filter Data -->
    <table id="tableData" class="table table-striped jambo_table bulk_action">
        <thead>
            <tr>
                <th>No</th>
                <th>Project</th>
                <th>Isu</th>
                <th>Permasalahan</th>
                <th>Tindak Lanjut</th>
                <th>Level</th>
                <th>Status</th>
                <th style="display:none"></th>
                <th style="display:none"></th>
                <th style="display: none"></th>
                <th>Category</th>

                <th>Created By</th>
                <th>Finished By</th>
                <th>Aksi</th>

            </tr>
        </thead>
        <tbody>
            <?php 
                foreach ($issue as $index => $row) {                  
                  ?>
            <tr class="odd gradeX">
                <td><?= $index+1?></td>
                <td><?= $row->project_name;?></td>
                <td><?= $row->name ;?></td>
                <td class="text-center">
                    <?php 
                        if($row->issue == null){
                         echo "-";
                        }
                        else{
                         echo $row->issue;
                        }
                        ?>
                </td>
                <td class="text-center">
                    <?php 
                        if($row->action == null){
                         echo "-";
                        }
                        else{
                         echo $row->action;
                        }
                        ?>
                </td>
                <td>
                    <?php if($row->level == "major"){?>
                    <span style="font-size: 12px" class="label label-danger text-center"><?= $row->level ;?></span>
                    <?php } else {?>
                    <span style="font-size: 12px" class="label label-warning text-center"><?= $row->level ;?></span>
                    <?php }?>
                </td>
                <td>
                    <?php if($row->status == "open"){?>
                    <span style="font-size: 12px" class="label label-success text-center"><?= $row->status ;?></span>
                    <?php } else {?>
                    <span style="font-size: 12px" class="label label-warning text-center"><?= $row->status ;?></span>
                    <?php }?>
                </td>
                <td style="display:none"><?php echo $row->level?></td>
                <td style="display:none"><?php echo $row->status?></td>
                <td style="display: none"></td>
                <td>
                    <?php if ($row->category==null){
                        ?> <p style="text-align: center;">-</p> <?php
                    } else {
                        echo $row->category;
                    } ?>
                </td>
                <td><?= $row->pic_name ;?></td>
                <td><?= $row->finished_name ;?></td>
                <?php if($row->pic != $this->session->userdata('pic')){ ?>
                <td><a href="#" class="btn btn-primary btn-sm"><i class="fa fa-arrow-right"></i> Close Issue</a></td>
                <?php } else {?>
                <td><a href="<?php echo site_url('issue/edit_issue/'.$row->id); ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a><br><br>
                    <a href="<?php echo site_url('issue/delete_issue/'.$row->id); ?>" class="btn btn-danger" onClick="return confirm('Yakin ingin menghapus Data ini?')"><i class="fa fa-trash"></i></a>
                </td>
                <?php } ?>
            </tr>
            <?php
                }
                ?>
        </tbody>
    </table>
</div>
</body>
</html>