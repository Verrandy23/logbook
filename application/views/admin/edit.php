<div class="panel panel-default">
    <div class="panel-heading">
        Edit Activity Project
    </div>
    <div class="panel-body">
        <form action="<?= site_url('Activityproject/update/'.$id)?>" method="post">
            <div class="form-group">
                <label>Pilih Project</label>
                <select name="project" class="form-control">
                    <option value="" disabled selected>Pilih Project</option>
                    <?php foreach ($project as $row): ?>
                    <option value="<?php echo $row->id ?>" <?php echo ($row->id == $project_id)?'selected="selected"':''?>><?php echo $row->name?></option>
                    <?php endforeach ?>
                </select>
            </div>
            <div class="form-group">
                <label>Pilih Kategori</label>
                <select name="category" class="form-control">
                    <option value="" disabled selected>Pilih Kategori</option>
                    <?php foreach ($category as $row): ?>
                    <option value="<?php echo $row->id ?>" <?php echo ($row->id == $category_id)?'selected="selected"':''?>><?php echo $row->name?></option>
                    <?php endforeach ?>
                </select>
            </div>
            <label>Tanggal Kegiatan Awal</label>
            <div class="form-group input-group">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>            
                <input type="date" name="start_date" class="form-control" value="<?= $start_date ?>">
            </div>
            <label>Tanggal Kegiatan Akhir</label>
            <div class="form-group input-group">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>            
                <input type="date" name="end_date" class="form-control" value="<?= $end_date ?>">
            </div>
            <label>Masukkan Judul Kegiatan</label>
            <div class="form-group">
                <input type="text" name="name_activity" class="form-control" value="<?= $name_activity ?>" placeholder="Judul Kegiatan" required>
            </div>
            <label>Masukkan Pembahasan</label>
            <div class="form-group">
                <textarea class="form-control" rows="3" name="discussion"><?= $discussion ?></textarea>
            </div>
            <div class="form-group">
                <label>Masukkan Tindak Lanjut</label>
                <textarea class="form-control" rows="3" name="action"><?= $action ?></textarea>
            </div>
            <label>Masukkan Keterangan</label>
            <div class="form-group">
                <input type="text" name="information" class="form-control" value="<?= $information ?>" placeholder="Keterangan" required>
            </div>
            <div class="modal-footer">
                <button class="btn btn-info" type="submit">Simpan</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
    $('#summernote').summernote();
    })
</script>
