<div class="panel panel-default">
    <div class="panel-heading">
        Edit Project
    </div>
    <div class="panel-body">
        <form action="<?= site_url('Project/update_project/'.$id)?>" method="post">
            <div class="form-group">
                <label>Pilih Master Project</label>
                <select name="project" class="form-control">
                    <option value="" disabled selected>Pilih Project</option>
                    <?php foreach ($master_project as $row): ?>
                    <option value="<?php echo $row->id ?>" <?php echo ($row->id == $master_project_id)?'selected="selected"':''?>><?php echo $row->project_code?></option>
                    <?php endforeach ?>
                </select>
            </div>
            <label>Nama Project</label>
            <div class="form-group">
                <input type="text" name="name" value="<?= $name?>" class="form-control" placeholder="Masukkan Nama Project" required>
            </div>
            <div class="modal-footer">
                <button class="btn btn-info" type="submit">Update</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>
</div>