<div class="panel panel-default">
    <div class="panel-heading">
        Edit Issue
    </div>
    <div class="panel-body">
        <form action="<?= site_url('Issue/update_issue/'.$id)?>" method="post">
            <div class="form-group">
                <label>Pilih Project</label>
                <select name="project" class="form-control">
                    <option value="" disabled selected>Pilih Project</option>
                    <?php foreach ($project as $id): ?>
                    <option value="<?php echo $id->id ?>" <?php echo ($id->id == $project_id)?'selected="selected"':''?>><?php echo $id->name?></option>
                    <?php endforeach ?>
                </select>
            </div>
            <label>Masukkan Nama Isu</label>
            <div class="form-group">
                <input type="text" name="name" value="<?= $name?>" class="form-control" placeholder="Judul Kegiatan" required>
            </div>
            <label>Permasalahan</label>
            <div class="form-group">
                <textarea class="form-control" name="issue"><?= $issue?></textarea>
            </div>
            <div class="form-group">
                <label>Tindak Lanjut </label>
                <textarea class="form-control" rows="3" name="action"><?= $action?></textarea>
            </div>
            <div class="form-group">
                <label>Level</label>
                <select name="level" class="form-control">
                    <option value="" disabled selected>Pilih Level</option>
                    <option value="<?= $level ?>" <?php echo ($level == "major")?'selected="selected"':''?>>Major</option>
                    <option value="<?= $level ?>" <?php echo ($level == "minor")?'selected="selected"':''?>>Minor</option>
                </select>
            </div>
            <div class="form-group">
                <label>Status</label>
                <select name="status" class="form-control">
                    <option value="" disabled selected>Pilih Status</option>
                    <option value="<?= $status ?>" <?php echo ($status == "open")?'selected="selected"':''?>>Open</option>
                    <option value="<?= $status ?>" <?php echo ($status == "close")?'selected="selected"':''?>>Close</option>
                </select>
            </div>
            <div class="modal-footer">
                <button class="btn btn-info" type="submit">Simpan</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>
</div>