<div class="panel panel-default">
    <div class="panel-heading">
        Edit Activity Plan
    </div>
    <div class="panel-body">
        <form action="<?= site_url('Activityplan/update_activity/'.$id)?>" method="post">
            <div class="form-group">
                <label>Pilih Project</label>
                <select name="projectid" class="form-control">
                   <option value="" disabled selected>Pilih Project</option>
                    <?php foreach ($project as $row): ?>
                    <option value="<?php echo $row->id ?>" <?php echo ($row->id == $project_id)?'selected="selected"':''?>><?php echo $row->name?></option>
                    <?php endforeach ?>
                </select>
            </div>
            <label>Aktifitas</label>
            <div class="form-group">
                <input type="text" name="name"  value="<?= $name?>" class="form-control" placeholder="Masukkan Aktifitas" required>
            </div>
            <label>Tanggal Rencana Start</label>
            <div class="form-group">
                <input type="date"  value="<?= $plan_start_date?>" name="plan_start_date" class="form-control" required>
            </div>
            <label>Tanggal Rencana End</label>
            <div class="form-group">
                <input type="date" value="<?= $plan_end_date?>" name="plan_end_date" class="form-control" required>
            </div>
            <label>Tanggal Pembuatan</label>
           <div class="form-group">
                <input type="date"  value="<?= $created_at?>" name="created_at" class="form-control" required>
            </div>
            <div class="form-group">
                <label>Deskripsi </label>
                <textarea class="form-control" rows="3" name="description" placeholder="Masukkan Catatan"> <?= $description?></textarea>
            </div>
            <div class="form-group">
                <label>Catatan </label>
                <textarea class="form-control" rows="3" name="note" placeholder="Masukkan Catatan"> <?= $note?></textarea>
            </div>
            <div class="form-group">
                <label>Status Realisasi</label>
                <select name="status_realization" class="form-control">
                    <option value="" disabled selected>Pilih Status</option>
                    <option value="<?= $status_realization ?>" <?php echo ($status_realization == "Rencana")?'selected="selected"':''?>>Rencana</option>
                    <option value="<?= $status_realization ?>" <?php echo ($status_realization == "on progress")?'selected="selected"':''?>>On Progress</option>
                    <option value="<?= $status_realization ?>" <?php echo ($status_realization == "terealisasi")?'selected="selected"':''?>>Terealisasi</option>
                </select>
            </div>
           
            <div class="modal-footer">
                <button class="btn btn-info" type="submit">Simpan</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>
</div>  