<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    ?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Logbook Web</title>
        <!-- BOOTSTRAP STYLES-->
        <link href="<?= base_url('assets/admin/css/bootstrap.css') ?>" rel="stylesheet" />
        <!-- FONTAWESOME STYLES-->
        <link href="<?= base_url('assets/admin/css/font-awesome.css')?>" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
        <link href="<?= base_url('assets/admin/css/custom.css')?>" rel="stylesheet" />
        <!-- GOOGLE FONTS-->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    </head>
    <body style="background-image: url(<?= base_url('assets/admin/img/bg.jpeg')?>); background-attachment: fixed;background-repeat: no-repeat; background-size: cover; background-position: center;
        ">
        <div class="container">
            <div class="row text-center ">
                <div class="col-md-12">
                    <br /><br />
                    <h2 style="color: white">Logbook Web Keproyekan</h2>
                    <h5 style="color: white">( Login yourself to get access )</h5>
                    <br />
                </div>
            </div>
            <div class="row ">
                <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <strong>   Enter Details To Login </strong>  
                        </div>
                        <div class="panel-body">
                            <form method="POST" action="<?= site_url('login/login') ?>">
                                <br />
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-tag"  ></i></span>
                                    <input id="nip" type="text" class="form-control" placeholder="Your Nip" required name="nip" />
                                </div>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock"  ></i></span>
                                    <input id="password" type="password" class="form-control " required name="password"  placeholder="Your Password" />
                                </div>
                                
                                <button type="submit" class="btn btn-primary">
                                Login Now
                                </button>
                                <hr />
                                Not register ? <a href="#" >Please contact admin. </a> 
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
        <!-- JQUERY SCRIPTS -->
        <script src="<?= base_url('assets/admin/js/jquery-1.10.2.js')?>"></script>
        <!-- BOOTSTRAP SCRIPTS -->
        <script src="<?= base_url('assets/admin/js/bootstrap.min.js')?>"></script>
        <!-- METISMENU SCRIPTS -->
        <script src="<?= base_url('assets/admin/js/jquery.metisMenu.js')?>"></script>
        <!-- CUSTOM SCRIPTS -->
        <script src="<?= base_url('assets/admin/js/custom.js')?>"></script>
    </body>
</html>