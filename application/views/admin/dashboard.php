<style>
    .order-card {
    color: #fff;
    }
    .bg-c-blue {
    background: linear-gradient(45deg,#4099ff,#73b4ff);
    }
    .bg-c-green {
    background: linear-gradient(45deg,#2ed8b6,#59e0c5);
    }
    .bg-c-yellow {
    background: linear-gradient(45deg,#FFB64D,#ffcb80);
    }
    .bg-c-pink {
    background: linear-gradient(45deg,#FF5370,#ff869a);
    }
    .bg-c-purple {
    background: linear-gradient(45deg,#cd84f1,#c56cf0);
    }
    .bg-c-rose {
    background: linear-gradient(45deg,#f3a683,#f19066);
    }
    .card {
    border-radius: 5px;
    -webkit-box-shadow: 0 1px 2.94px 0.06px rgba(4,26,55,0.16);
    box-shadow: 0 1px 2.94px 0.06px rgba(4,26,55,0.16);
    border: none;
    margin-bottom: 30px;
    -webkit-transition: all 0.3s ease-in-out;
    transition: all 0.3s ease-in-out;
    }
    .card .card-block {
    padding: 25px;
    }
    .order-card i {
    font-size: 26px;
    }
    .f-left {
    float: left;
    }
    .f-right {
    float: right;
    }
</style>
<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-dashboard"></i> Dashboard
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-4 col-xl-4">
                    <div class="card bg-c-blue order-card">
                        <div class="card-block">
                            <?php
                                if ($role_user == 003){
                                    
                                    ?>  
                            <h6 class="m-b-20"><?= $project->project; ?></h6>
                            
                            
                                <h2 class="text-right"><i class="fa fa-train f-left"></i>
                                    Project<span></span></h2>
                                    
                                <?php
                                } else { 
                                foreach ($total_project as $row) {                  
                                  ?>
                            <h6 class="m-b-20">Total Project</h6>
                            <h2 class="text-right"><i class="fa fa-train f-left"></i><span><?= $row->total ;?></span></h2>
                            <?php 
                                }
                            }
                                ?>
                            <p class="m-b-0">Train Project</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-xl-4">
                    <div class="card bg-c-green order-card">
                        <div class="card-block">
                            <h6 class="m-b-20">Total Activity Plan</h6>
                             <?php
                                if ($role_user == 003){
                                    
                                    ?>
                                    <h2 class="text-right"><i class="fa fa-book f-left"></i><span><?= $user->total ;?></span></h2>  
                            <?php 
                        }
                             else {
                                foreach ($total_activity as $row) {                  
                                  ?>
                            <h2 class="text-right"><i class="fa fa-book f-left"></i><span><?= $row->total ;?></span></h2>
                            <?php 
                                } 
                            }
                                ?>
                            <p class="m-b-0">Activity Record Plan</p>
                        </div>
                    </div>
                </div>
                <?php if ($role_user != 001){ 
                            } else{ ?>
                <div class="col-md-4 col-xl-4">
                    <div class="card bg-c-yellow order-card">
                        <div class="card-block">
                            
                            <h6 class="m-b-20">Total Attachment</h6>
                            <?php 
                                foreach ($total_attachment as $row) {                  
                                  ?>
                            <h2 class="text-right"><i class="fa fa-paperclip f-left"></i><span><?= $row->total ;?></span></h2>
                            <p class="m-b-0">Attachment Activity</p>
                            
                        </div>
                    </div>
                </div>
                <?php 
                                }
                            }
                                ?>
 
                <div class="col-md-4 col-xl-4">
                    <div class="card bg-c-pink order-card">
                        <div class="card-block">
                            <h6 class="m-b-20">Total Issue</h6>
                            <?php 
                                foreach ($total_issue as $row) {                  
                                  ?>
                            <h2 class="text-right"><i class="fa fa-credit-card f-left"></i><span><?= $row->total ;?></span></h2>
                            <p class="m-b-0">Issue All Project</p>
                            <?php 
                                }
                                ?>
                        </div>
                    </div>
                </div>



                <div class="col-md-4 col-xl-4">
                    <div class="card bg-c-purple order-card">
                       <div class="card-block">
                        <?php if($role_user==003){
                            ?>
                            <h6 class="m-b-20">Total Anggota</h6>
                            
                            <h2 class="text-right"><i class="fa fa-user f-left"></i>
                                <?= $get_anggota->total; ?><span></span></h2>
                                <p class="m-b-0"> Project Manager </p>

                            <?php
                            } else { 
                                foreach ($total_user as $row) {   

                                  ?>
                                    <h6 class="m-b-20">Total User</h6>
                            
                            <h2 class="text-right"><i class="fa fa-user f-left"></i>
                                <?= $row->total; ?><span></span></h2>

                            <?php 
                            }
                                }
                                ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <figure class="highcharts-figure">
        <div id="container"></div>
        <p class="highcharts-description">
        </p>
    </figure>

    <figure class="highcharts-figure">
        <div id="container2"></div>
        <p class="highcharts-description">
        </p>
    </figure>
    <figure class='open_close'>
        <canvas id='myChart'></canvas>
    </figure>
    <script type="text/javascript">
            
        var ctx = document.getElementById("myChart").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: ["Open", "Closed"],
                datasets: [{
                    label: '',
                    data: [
                    <?php echo $dashboard_open_issue?>
                    , 
                    <?php echo $dashboard_close_issue ?>
                    ],
                    backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)'
                    ],
                    borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                },
                title:{

                    display: true,
                    text:'Rekap Status Issue',
                }
            }
        });
    
    </script>
</div>
</div>