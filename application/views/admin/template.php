<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="assets/img/favicon.ico">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>Logbook Web Keproyekan</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />
        <!-- Bootstrap core CSS     -->
        <link href="<?= base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet" />
        <!-- Animation library for notifications   -->
        <link href="<?= base_url('assets/css/animate.min.css')?>" rel="stylesheet"/>
        <!--  Light Bootstrap Table core CSS    -->
        <link href="<?= base_url('assets/css/light-bootstrap-dashboard.css')?>" rel="stylesheet"/>
        <link href="<?= base_url('assets/css/demo.css')?>" rel="stylesheet" />
        <link href="<?= base_url('assets/css/bootstrap-datetimepicker.css')?>" rel="stylesheet" />
        <link href="<?= base_url('assets/css/datepicker.css')?>" rel="stylesheet" />
        <link href="<?= base_url('assets/js/dataTables/dataTables.bootstrap.css')?>" rel="stylesheet" />
        <!--     Fonts and icons     -->
        <script type="text/javascript" src="<?= base_url('assets/js/Chart.js') ?>"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
        <link href="<?= base_url('assets/css/pe-icon-7-stroke.css')?>" rel="stylesheet" />
        <style>
            #container {
            height: 400px; 
            }
            .highcharts-figure, .highcharts-data-table table {
            min-width: 310px; 
            max-width: 800px;
            margin: 1em auto;
            }
            .highcharts-data-table table {
            font-family: Verdana, sans-serif;
            border-collapse: collapse;
            border: 1px solid #EBEBEB;
            margin: 10px auto;
            text-align: center;
            width: 100%;
            max-width: 500px;
            }
            .highcharts-data-table caption {
            padding: 1em 0;
            font-size: 1.2em;
            color: #555;
            }
            .highcharts-data-table th {
            font-weight: 600;
            padding: 0.5em;
            }
            .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
            padding: 0.5em;
            }
            .highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
            background: #f8f8f8;
            }
            .highcharts-data-table tr:hover {
            background: #f1f7ff;
            }
        </style>
    </head>
    <body>
        <div class="wrapper">
            <div class="sidebar" data-color="red" data-image="<?= base_url('assets/img/sidebar-4.jpg')?>">
                <!--   you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple" -->
                <div class="sidebar-wrapper">
                    <div class="logo">
                        <a href="http://www.creative-tim.com" class="simple-text">
                        Logbook Web
                        </a>
                    </div>
                    <ul class="nav">
                        <li class="active">
                            <a href="<?php echo site_url('dashboard')?>">
                                <i class="pe-7s-graph"></i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('project')?>">
                                <i class="pe-7s-menu"></i>
                                <p>Project </p>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('ActivityProject')?>">
                                <i class="pe-7s-drawer"></i>
                                <p>Activity Project</p>
                            </a>
                        </li>
                        <li>
                            <a onclick="location.href = 'http://api-pm.inka.int/report/?params=YWt1IGFkYWxhaCBzZW9yYW5nIHN1cGVybWFuOmFrdSBhZGFsYWggc2VvcmFuZyBzdXBlcm1hbiBkYW4gYmF0bWFu'">
                                <i class="pe-7s-note2"></i>
                                <p>Report Activity Project</p>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('issue')?>">
                                <i class="pe-7s-news-paper"></i>
                                <p>Manage Issue</p>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('ActivityPlan')?>">
                                <i class="pe-7s-note"></i>
                                <p>Manage Activity Plan</p>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('user')?>">
                                <i class="pe-7s-user"></i>
                                <p>Manage User</p>
                            </a>
                        </li>
                        <li class="active-pro">
                            <a href="<?php echo site_url('dashboard')?>">
                                <i></i>
                                <p>PT INKA (Persero)</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="main-panel">
                <nav class="navbar navbar-default navbar-fixed">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            </button>
                            <div style="color: #4b4b4b;
                                padding: 5px;margin-top: 10px;
                                font-size: 13px;">
                                <a href="#" style="text-decoration: none" class="clock24" id="tz24-1573520845-c1108-eyJob3VydHlwZSI6IjI0Iiwic2hvd2RhdGUiOiIxIiwic2hvd3NlY29uZHMiOiIxIiwic2hvd3RpbWV6b25lIjoiMSIsInR5cGUiOiJkIiwibGFuZyI6ImVuIn0=" title="World Clock :: Jakarta" target="_blank" rel="nofollow"><span style="color: #4b4b4b">Date Now</span></a>
                                <script type="text/javascript" src="//w.24timezones.com/l.js" async></script>
                            </div>
                        </div>
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <li>
                                    <a href="<?php echo site_url('login/logout')?>">
                                        <p><img width="11px" src="<?= base_url('assets/img/online.png')?>"> Hello <?php echo $this->session->userdata('nip') ?>, <i class="fa fa-sign-out"></i> Log out</p>
                                    </a>
                                </li>
                                <li class="separator hidden-lg hidden-md"></li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <?php $this->load->view($main_content); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <footer class="footer">
                    <div class="container-fluid">
                        <p class="copyright pull-right">
                            &copy; <script>document.write(new Date().getFullYear())</script> <a href="http://www.creative-tim.com">Divisi Keproyekan</a> 
                        </p>
                    </div>
                </footer>
            </div>
        </div>
    </body>
    <!--   Core JS Files   -->
    <script src="<?= base_url('assets/js/jquery-1.10.2.js')?>" type="text/javascript"></script>
    <script src="<?= base_url('assets/js/bootstrap.min.js')?>" type="text/javascript"></script>
    <!--  Charts Plugin -->
    <script src="<?= base_url('assets/js/chartist.min.js')?>"></script>
    <!--  Notifications Plugin    -->
    <script src="<?= base_url('assets/js/bootstrap-notify.js')?>"></script>
    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
    <script src="<?= base_url('assets/js/light-bootstrap-dashboard.js')?>"></script>
    <!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
    <script src="<?= base_url('assets/js/demo.js')?>"></script>
    <script src="<?= base_url('assets/js/dataTables/jquery.dataTables.js')?>"></script>
    <script src="<?= base_url('assets/js/dataTables/dataTables.bootstrap.js')?>"></script>
    <script src="<?= base_url('assets/js/bootstrap-datetimepicker.js')?>"></script>
    <script src="<?= base_url('assets/js/bootstrap-datepicker.js')?>"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/highcharts-3d.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
   

    <script>
    //Filter 
    $(document).ready(function() {
            var table=$('#tableData').DataTable({
                initComplete: function () {
                }
        });
        $(".filterhead").each(function (i) {
                    if (i != 15  && i != 16 ) {
                        var select = $('<select class="form-control" style="margin:15px"><option value="">All Data</option></select>')
                            .appendTo($(this).empty())
                            .on('change', function () {
                                var term = $(this).val();
                                table.column(i).search(term, false, false).draw();
                            });
                        table.column(i).data().unique().sort().each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>')
                        });
                    } else {
                        $(this).empty();
                    }
                });
        } );
    </script>
    <script>
    function set_mouseover(id) {
    jQuery('#mouseover_cell_id').val(id);
    }
    </script>
    <script>
        Highcharts.chart('container', {
        chart: {
        type: 'column',
        options3d: {
            enabled: true,
            alpha: 10,
            beta: 25,
            depth: 70
        }
        },
        title: {
        text: 'Rekap Total Aktifitas Project Tahun 2019'
        },
        subtitle: {
        text: 'Total aktifitas project per bulan'
        },
        plotOptions: {
        column: {
            depth: 25
        }
        },
        xAxis: {
        categories: Highcharts.getOptions().lang.shortMonths,
        labels: {
            skew3d: true,
            style: {
                fontSize: '16px'
            }
        }
        },
        yAxis: {
        title: {
            text: null
        }
        },
        series: [{
        name: 'Aktifitas',
        data: [<?php foreach($dashboard as $row){
            if($row->month != null){
            echo $row->jumlah;
            echo ","; 
            }
            }?>
        ]
        }]
        });
    </script>
    <script>
        Highcharts.chart('container2', {
        chart: {
        type: 'column',
        options3d: {
            enabled: true,
            alpha: 10,
            beta: 25,
            depth: 70
        }
        },
        title: {
        text: 'Rekap Total Aktifitas Project Tahun 2020'
        },
        subtitle: {
        text: 'Total aktifitas project per bulan'
        },
        plotOptions: {
        column: {
            depth: 25
        }
        },
        xAxis: {
        categories: Highcharts.getOptions().lang.shortMonths,
        labels: {
            skew3d: true,
            style: {
                fontSize: '16px'
            }
        }
        },
        yAxis: {
        title: {
            text: null
        }
        },
        series: [{
        name: 'Aktifitas',
        data: [<?php foreach($dashboard_new as $row){
            if($row->month != null){
            echo $row->jumlah;
            echo ","; 
            }
            }?>
        ]
        }]
        });
    </script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#project_role').change(function(){
        
          var id=$(this).val();
            $.ajax({
                url : "<?php echo base_url();?>index.php/user/get_user_role",
                method : "POST",
                data : {id: id},
                async : false,
                dataType : 'json',
                success: function(data){
                    if(data == 0){
                    var html = '';
                    var i;
                        html += '<option value="" selected>Pilih Role User</option>';
                        html += '<option value="001">Admin</option>';
                        html += '<option value="002">Management</option>';
                        html += '<option value="005">Staff Keproyekan</option>';

                    
                    $('.user_role').html(html);
                    } else {
                    
                    var html2 = '';
                    var i;
                    
                        html2 += '<option value="" selected>Pilih Role User</option>';
                        html2 += '<option value="003">Project Manager</option>';
                        html2 += '<option value="004">Staff Project Manager</option>';
                    
                    $('.user_role').html(html2);

                    }
                     
                }
            });
        });
    });
</script>
</html>