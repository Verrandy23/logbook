<div class="panel panel-default">
    <div class="panel-heading">
        Create Activity Plan
    </div>
    <div class="panel-body">
        <form action="<?= site_url('Activityplan/insert_data_activity')?>" method="post">
            <div class="form-group">
                <label>Pilih Project</label>
                <select name="projectid" class="form-control">
                    <option value="" disabled selected>Pilih Project</option>
                    <?php 
                        foreach ($project as $row) {                  
                          ?>
                    <option value=" <?= $row->id;?>"><?= $row->name;?></option>
                    <?php
                        }
                        ?>
                </select>
            </div>
            <label>Aktifitas</label>
            <div class="form-group">
                <input type="text" name="name" class="form-control" placeholder="Masukkan Aktifitas" required>
            </div>
            <label>Tanggal Rencana Start</label>
            <div class="form-group">
                <input type="date" name="plan_start_date" class="form-control" required>
            </div>
            <label>Tanggal Rencana End</label>
            <div class="form-group">
                <input type="date" name="plan_end_date" class="form-control" required>
            </div>
            <label>Tanggal Pembuatan</label>
           <div class="form-group">
                <input type="date" name="created_at" class="form-control" required>
            </div>
            <div class="form-group">
                <label>Deskripsi </label>
                <textarea class="form-control" rows="3" name="description" placeholder="Masukkan Deskripsi"></textarea>
            </div>
            <div class="form-group">
                <label>Catatan </label>
                <textarea class="form-control" rows="3" name="note" placeholder="Masukkan Catatan"></textarea>
            </div>
            <div class="form-group">
                <label>Status Realisasi</label>
                <select name="status_realization" class="form-control">
                    <option value="" disabled selected>Pilih Status</option>
                    <option value="Rencana">Rencana</option>
                    <option value="on progress">On Progress</option>
                    <option value="terealisasi">Terealisasi</option>
                </select>
            </div>
           
            <div class="modal-footer">
                <button class="btn btn-info" type="submit">Simpan</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>
</div>