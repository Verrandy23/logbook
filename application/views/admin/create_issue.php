<div class="panel panel-default">
    <div class="panel-heading">
        Create Activity Project
    </div>
    <div class="panel-body">
        <form action="<?= site_url('Issue/insert_data_issue')?>" method="post">
            <div class="form-group">
                <label>Pilih Project</label>
                <select name="projectid" class="form-control" id="projectid" onclick="">
                    <option value="" disabled selected>Pilih Project</option>
                    <?php 
                        foreach ($project as $row) {                  
                          ?>
                    <option value=" <?= $row->id;?>"><?= $row->name;?></option>
                    <?php
                        }
                        ?>
                </select>
                <div>
                   <p  class='invalid-feedback' id='errorP'></p>
                </div>
            </div>
             <div class="form-group">
                <label>Pilih Category Issue</label>
                <select name="cat_issue" class="form-control">
                    <option value="" disabled selected hidden="">Category</option>
                    <?php 
                        foreach ($category as $row) {                  
                          ?>
                    <option value=" <?= $row->id_category_issues;?>"><?= $row->name_category;?></option>
                    <?php
                        }
                        ?>
                </select>
            </div>
            <label>Masukkan Nama Issues</label>
            <div class="form-group">
                <input type="text" name="name" class="form-control" id='name_issue' placeholder="Judul Kegiatan" required onchange="">
            </div>
            <label>Permasalahan</label>
            <div class="form-group">
                <textarea class="form-control" name="issue"></textarea>
            </div>
            <div class="form-group">
                <label>Tindak Lanjut </label>
                <textarea class="form-control" rows="3" name="action"></textarea>
            </div>
            <div class="form-group">
                <label>Level</label>
                <select name="level" class="form-control">
                    <option value="" disabled selected>Pilih Level</option>
                    <option value="major">Major</option>
                    <option value="minor">Minor</option>
                </select>
            </div>
            <div class="form-group">
                <label>Status</label>
                <select name="status" class="form-control">
                    <option value="" disabled selected>Pilih Status</option>
                    <option value="open">Open</option>
                    <option value="close">Close</option>
                </select>
            </div>
            <div class="modal-footer">
                <button class="btn btn-info" type="submit" id='simpan'>Simpan</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

</script>
