<div class="row">
    <div class="col-md-6 col-sm-12 col-xs-12">
        <div class="chat-panel panel panel-default chat-boder chat-panel-head" >
            <div class="panel-heading">
                <i class="fa fa-file fa-fw"></i>
                Attachment 
                <div class="btn-group pull-right">
                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-chevron-down"></i>
                    </button>
                    <ul class="dropdown-menu slidedown">
                        <li>
                            <a href="#">
                            <i class="fa fa-refresh fa-fw"></i>Refresh Attachment
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="panel-body">
                <ul class="chat-box">
                    <?php 
                        foreach ($attachment as $data) {                  
                          ?>
                    <?php if ($data->format == "docx") { ?>     
                    <li class="left clearfix">
                        <span class="chat-img pull-left">
                        <img src="<?= base_url('assets/admin/img/doc.png')?>" alt="User" class="img-circle"/>
                        </span>
                        <div class="chat-body">
                            <strong ><?= $data->attachment?></strong>
                            <small class="pull-right text-muted">
                            <i class="fa fa-trash-o fa-fw"></i><a href="<?php echo site_url('actvityproject/delete_attachment/'.$data->attachment.'/'.$data->id); ?>">Delete File</a>
                            </small>                                      
                            <p>
                                <a href="<?php echo site_url('actvityproject/vd_attachment/'.$data->attachment); ?>"> Lihat atau Download disini..</a>
                            </p>
                        </div>
                    </li>
                    <?php } ?>
                    <?php if ($data->format == "png" || $data->format == "jpg" || $data->format == "jpeg") { ?>     
                    <li class="left clearfix">
                        <span class="chat-img pull-left">
                        <img src="<?= base_url('assets/admin/img/pictures.png')?>" alt="User" class="img-circle"/>
                        </span>
                        <div class="chat-body">
                            <strong ><?= $data->attachment?></strong>
                            <small class="pull-right text-muted">
                            <i class="fa fa-trash-o fa-fw"></i><a href="<?php echo site_url('activityproject/delete_attachment/'.$data->attachment.'/'.$data->id); ?>">Delete File</a>
                            </small>                                      
                            <p>
                                <a href="<?php echo site_url('activityproject/vd_attachment/'.$data->attachment); ?>"> Lihat atau Download disini..</a>
                            </p>
                        </div>
                    </li>
                    <?php } ?>
                    <?php if ($data->format == "pdf") { ?>       
                    <li class="left clearfix">
                        <span class="chat-img pull-left">
                        <img src="<?= base_url('assets/admin/img/pdf.png')?>" alt="User" class="img-circle" />
                        </span>
                        <div class="chat-body">
                            <strong ><?= $data->attachment?></strong>
                            <small class="pull-right text-muted">
                            <i class="fa fa-trash-o fa-fw"></i><a href="<?php echo site_url('activityproject/delete_attachment/'.$data->attachment.'/'.$data->id); ?>">Delete File</a>
                            </small>                                      
                            <p>
                                <a href="<?php echo site_url('activityproject/vd_attachment/'.$data->attachment); ?>"> Lihat atau Download disini..</a>
                            </p>
                        </div>
                    </li>
                    <?php } ?>
                    <?php } ?>
                </ul>
            </div>
            <div class="panel-footer">
                <form class="col s12" action="<?= site_url('activityproject/vu_attachment')?>" method="post" enctype="multipart/form-data">
                    <input type="file" name="file">
                    <input type="hidden" name="id" value="<?= $id?>">
                    <br>
                    <span class="input-group-btn">
                    <button class="btn btn-warning btn-sm" id="btn-chat">
                    Add Attachment
                    </button>
                    </span>
                </form>
            </div>
        </div>
    </div>
</div>
