
<div class="col-md-12 col-sm-12 col-xs-12">
    <h3 class="text-center">Data Activity</h3>
    <hr>
</div>
<div class="table-responsive">
    <p>
        <a style="margin-left: 1%" href="<?php echo site_url('activityproject/create')?>"><button class="btn btn-primary"><i class="fa fa-plus"></i> Input Data </button></a>
    </p>

    <?php $this->view('messages') ?>

    <!-- Filter Data -->
    <table> 
        <tr>
            <tr>
            <td><h6 style="margin-left:12%" for="filter">Filter By Project</h6></td>
            <td><h6 style="margin-left:16%" for="filter">Filter By Kategori</h6></td>

            </tr>
            <th class="filterhead" style="display:none"></th>
            <th class="filterhead"></th><th class="filterhead" style="display:none"></th>
            <th class="filterhead" style="display:none"></th><th class="filterhead" style="display:none"></th>
            <th class="filterhead" style="display:none"></th><th class="filterhead" style="display:none"></th>
            <th class="filterhead" style="display:none"></th>
            <th class="filterhead"></th>
            
        </tr>
    </table>
    <!-- End Filter Data -->

    <table id="tableData" class="table table-striped jambo_table bulk_action">
        <thead>
            <tr>
                <th>No</th>
                <th>Project</th>
                <th>Kegiatan</th>
                <th>Tanggal Kegiatan</th>
                <th>Pembahasan</th>
                <th>Tindak Lanjut</th>
                <th>Keterangan</th>
                <th>PIC</th>
                <th>Kategori</th>
                <th>Attachment</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                foreach ($record as $index => $row) {                  
                  ?>
            <tr class="odd gradeX">
                <td><?= $index+1;?></td>
                <td><?= $row->project_name ;?></td>
                <td><?= $row->name_activity;?></td>
                <td><?= $row->start_date;?> <i>sampai</i> <?= $row->end_date ;?></td>
                <td style="text-align:justify;">
                    <?php 
                        if($row->discussion == null){
                         echo "<span style='text-align:center'> - </span>";
                        }
                        else{
                         echo $row->discussion;
                        }
                        ?>
                </td>
                <td style="text-align:justify;">
                    <?php 
                        if($row->action == null){
                         echo "-";
                        }
                        else{
                         echo $row->action;
                        }
                        ?>
                </td>
                <td><?= $row->information ;?></td>
                <td><?= $row->user_name ;?></td>
                <td><?= $row->category_name;?></td>
                <td><a class="btn btn-success btn-sm" href="<?php echo site_url('activityproject/attachment/'.$row->id); ?>">
                    <i class="fa fa-arrow-right"></i> Lihat Attachment</a>
                </td>
                <?php if($row->pic != $this->session->userdata('pic')){ ?>
                <td><a href="#" class="btn btn-primary-light btn-sm">Not pic</a></td>
                <?php } else {?>
                <td><a href="<?php echo site_url('ActivityProject/edit/'.$row->id); ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                </td>
                <?php } ?>
            </tr>
            <?php
                }
                ?>
        </tbody>
    </table>
</div>
</body>
</html>