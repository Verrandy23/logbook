<div class="panel panel-default">
    <div class="panel-heading">
        Create Activity Project
    </div>
    <div class="panel-body">
        <form action="<?= site_url('User/insert_data_user')?>" method="post">
            <div class="form-group">
                <label>Pilih Project</label>
                <select name="project_to_pm" class="form-control" id="project_role">
                    <option value="" disabled selected>Pilih Project</option>
                    <option value="0">Keproyekan</option>
                    <?php 
                        foreach ($project as $row) {                  
                          ?>
                    <option value=" <?= $row->id;?>"><?= $row->name;?></option>
                    <?php
                        }
                        ?>
                </select>
            </div>
            <label>NIP</label>
            <div class="form-group">
                <input type="text" name="nip" class="form-control" placeholder="Masukkan NIP" required>
            </div>
            <label>Nama</label>
            <div class="form-group">
                <input type="text" name="name" class="form-control" placeholder="Masukkan Nama" required>
            </div>
            <label>Password</label>
            <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="Masukkan Password" required>
            </div>
            <div class="form-group">
                <label>Jenis Kelamin</label>
                <select name="gender" class="form-control">
                    <option value="" disabled selected>Pilih Jenis Kelamin</option>
                    <option value="L">Laki - laki</option>
                    <option value="P">Perempuan</option>
                </select>
            </div>
            <label>Jabatan</label>
            <div class="form-group">
                <input type="text" name="position" class="form-control" placeholder="Masukkan Jabatan" required>
            </div>
            <div class="form-group">
                <label>Role User</label>
                <select name="role_user" class="form-control user_role">
                    <option value="" disabled selected>Pilih Role User</option>
                    
                </select>
            </div>
            <div class="modal-footer">
                <button class="btn btn-info" type="submit">Simpan</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>
</div>