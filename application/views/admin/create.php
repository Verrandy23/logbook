<div class="panel panel-default">
    <div class="panel-heading">
        Create Activity Project
    </div>
    <div class="panel-body">
        <form action="<?= site_url('Activityproject/insert_data')?>" method="post">
            <div class="form-group">
                <label>Pilih Project</label>
                <select name="project" class="form-control">
                    <option value="" disabled selected>Pilih Project</option>
                    <?php 
                        foreach ($project as $row) {                  
                          ?>
                    <option value=" <?= $row->id;?>"><?= $row->name;?></option>
                    <?php
                        }
                        ?>
                </select>
            </div>
            <div class="form-group">
                <label>Pilih Kategori</label>
                <select name="category" class="form-control">
                    <option value="" disabled selected>Pilih Kategori</option>
                    <?php 
                        foreach ($category as $row) {                  
                          ?>
                    <option value="<?= $row->id;?>"><?= $row->name;?></option>
                    <?php
                        }
                        ?>
                </select>
            </div>
            <label>Tanggal Kegiatan Awal</label>
            <div class="form-group input-group">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>            
                <input type="date" name="start_date" class="form-control" placeholder="YYYY-MM-DD">
            </div>
            <label>Tanggal Kegiatan Akhir</label>
            <div class="form-group input-group">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>            
                <input type="date" name="end_date" class="form-control" placeholder="YYYY-MM-DD">
            </div>
            <label>Masukkan Judul Kegiatan</label>
            <div class="form-group">
                <input type="text" name="name_activity" class="form-control" placeholder="Judul Kegiatan" required>
            </div>
            <label>Masukkan Pembahasan</label>
            <div class="form-group">
                <textarea class="form-control" name="discussion"></textarea>
            </div>
            <div class="form-group">
                <label>Masukkan Tindak Lanjut</label>
                <textarea class="form-control" rows="3" name="action"></textarea>
            </div>
            <label>Masukkan Keterangan</label>
            <div class="form-group">
                <input type="text" name="information" class="form-control" placeholder="Keterangan" required>
            </div>
            <div class="modal-footer">
                <button class="btn btn-info" type="submit">Simpan</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>
</div>
