<div class="panel panel-default">
    <div class="panel-heading">
        Create Activity Project
    </div>
    <div class="panel-body">
        <form action="<?= site_url('Project/insert_data_project')?>" method="post">
            <div class="form-group">
                <label>Pilih Master Project</label>
                <select name="master_project_id" class="form-control">
                    <option value="" disabled selected>Pilih Master Project</option>
                    <?php 
                        foreach ($master_project as $row) {                  
                          ?>
                    <option value=" <?= $row->id;?>"><?= $row->project_code;?></option>
                    <?php
                        }
                        ?>
                </select>
            </div>
            <label>Nama Project</label>
            <div class="form-group">
                <input type="text" name="name" class="form-control" placeholder="Masukkan Nama Project" required>
            </div>
            
            <div class="modal-footer">
                <button class="btn btn-info" type="submit">Simpan</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>
</div>