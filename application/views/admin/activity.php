
<div class="col-md-12 col-sm-12 col-xs-12">
    <h4 class="text-center">Data Activity Plan</h4>
    <hr>
</div>
<div class="table-responsive">
    <p>
        <a style="margin-left: 1%" href="<?php echo site_url('ActivityPlan/create_activity')?>"><button class="btn btn-primary"><i class="fa fa-plus"></i> Input Data </button></a>
    </p>

    <?php $this->view('messages') ?>

    <!-- Filter Data -->
        <table> 
        <tr>
            <tr>
                <td><h6 style="margin-left:12%;margin-top:6%" for="filter">Filter By Project</h6></td>
                <td><h6 style="margin-left:16%;margin-top:6%" for="filter">Filter By Status</h6></td>
                <td><h6 style="margin-left:16%;margin-top:6%" for="filter">Filter By Rencana Start</h6></td>
                <td><h6 style="margin-left:16%;margin-top:6%" for="filter">Filter By Rencana End</h6></td>
            </tr>
            <th class="filterhead" style="display:none"></th>
            <th class="filterhead"></th><th class="filterhead" style="display:none"></th>
            <th class="filterhead" style="display:none"></th><th class="filterhead" style="display:none"></th>
            <th class="filterhead" style="display:none"></th><th class="filterhead" style="display:none"></th>
            <th class="filterhead"></th><th class="filterhead"></th><th class="filterhead"></th>
            
        </tr>
    </table>
    <!-- End Filter Data -->
    <table id="tableData" class="table table-striped jambo_table bulk_action">
        <thead>
            <tr>
                <th>No</th>
                <th>Project</th>
                <th>Aktifitas</th>
                <th>Rencana Start</th>
                <th>Rencana End</th>
                <th>Deskripsi</th>
                <th>Status</th>
                <th style="display:none"></th>
                <th style="display:none"></th>
                <th style="display:none"></th>
                <th class="text-center">Catatan</th>
                <th>PIC</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                foreach ($activity as $index => $row) {                  
                  ?>
            <tr class="odd gradeX">
                <td><?= $index+1?></td>
                <td><?= $row->project_name;?></td>
                <td><?= $row->name ;?></td>
                <td><?= $row->plan_start_date ;?></td>
                <td><?= $row->plan_end_date ;?></td>
                <td><?= $row->description ;?></td>
                <td>
                    <?php if($row->status_realization == "Rencana"){?>
                        <span style="font-size: 12px" class="label label-primary text-center"><?= $row->status_realization ;?></span>
                    <?php } else if($row->status_realization == "on progress") {?>
                        <span style="font-size: 12px" class="label label-warning text-center"><?= $row->status_realization ;?></span>
                    <?php } else {?>
                        <span style="font-size: 12px" class="label label-success text-center"><?= $row->status_realization ;?></span>
                    <?php }?>
                </td>
                <td style="display:none"><?= $row->status_realization ;?></td>
                <td style="display:none">
                    <?php
                        $date = date_create($row->plan_start_date);
                        $string =  date_format($date, "j F Y");
                        if (stristr($string, 'January') !== FALSE) {
                            echo 'January';
                        } else if (stristr($string, 'February') !== FALSE) {
                            echo 'February';
                        } else if (stristr($string, 'March') !== FALSE) {
                            echo 'March';
                        } else if (stristr($string, 'April') !== FALSE) {
                            echo 'April';
                        } else if (stristr($string, 'May') !== FALSE) {
                            echo 'May';
                        } else if (stristr($string, 'June') !== FALSE) {
                            echo 'June';
                        } else if (stristr($string, 'July') !== FALSE) {
                            echo 'July';
                        } else if (stristr($string, 'August') !== FALSE) {
                            echo 'August';
                        } else if (stristr($string, 'September') !== FALSE) {
                            echo 'September';
                        } else if (stristr($string, 'October') !== FALSE) {
                            echo 'October';
                        } else if (stristr($string, 'November') !== FALSE) {
                            echo 'November';
                        } else if (stristr($string, 'December') !== FALSE) {
                            echo 'December';
                        }
                        ?>
                </td>

                <td style="display:none">
                    <?php
                        $date = date_create($row->plan_end_date);
                        $string =  date_format($date, "j F Y");
                        if (stristr($string, 'January') !== FALSE) {
                            echo 'January';
                        } else if (stristr($string, 'February') !== FALSE) {
                            echo 'February';
                        } else if (stristr($string, 'March') !== FALSE) {
                            echo 'March';
                        } else if (stristr($string, 'April') !== FALSE) {
                            echo 'April';
                        } else if (stristr($string, 'May') !== FALSE) {
                            echo 'May';
                        } else if (stristr($string, 'June') !== FALSE) {
                            echo 'June';
                        } else if (stristr($string, 'July') !== FALSE) {
                            echo 'July';
                        } else if (stristr($string, 'August') !== FALSE) {
                            echo 'August';
                        } else if (stristr($string, 'September') !== FALSE) {
                            echo 'September';
                        } else if (stristr($string, 'October') !== FALSE) {
                            echo 'October';
                        } else if (stristr($string, 'November') !== FALSE) {
                            echo 'November';
                        } else if (stristr($string, 'December') !== FALSE) {
                            echo 'December';
                        }
                        ?>
                </td>

                <td class="text-center">
                    <?php 
                        if($row->note == null){
                         echo "-";
                        }
                        else{
                         echo $row->note;
                        }
                        ?>
                </td>
                <td><?= $row->user_name ;?></td>
                <?php if($row->pic != $this->session->userdata('pic')){ ?>
                <td><a href="#" class="btn btn-primary-light btn-sm">Not pic</a></td>
                <?php } else {?>
                <td><a href="<?php echo site_url('activityplan/edit_activity/'.$row->id); ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a><br><br>
                    <a href="<?php echo site_url('activityplan/delete_activity/'.$row->id); ?>" class="btn btn-danger" onClick="return confirm('Yakin ingin menghapus Data ini?')"><i class="fa fa-trash"></i></a>
                </td>
                <?php } ?>
            </tr>
            <?php
                }
                ?>
        </tbody>
    </table>
</div>
</body>
</html>