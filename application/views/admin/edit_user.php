<div class="panel panel-default">
    <div class="panel-heading">
        Edit User
    </div>
    <div class="panel-body">
        <form action="<?= site_url('User/update_user/'.$id)?>" method="post">
            <div class="form-group">
                <label>Pilih Project To PM</label>
                <select name="project_to_pm" class="form-control" id="project_role">
                    <option value="" disabled selected>Pilih Project To PM</option>
                    <option value="" <?php echo ($project_to_pm == "")?'selected="selected"':''?>>Keproyekan</option>
                    <?php foreach ($project as $row): ?>
                    <option value="<?php echo $row->id ?>" <?php echo ($row->id == $project_to_pm)?'selected="selected"':''?>><?php echo $row->name?></option>
                    <?php endforeach ?>
                </select>
            </div>
            <label>Usename</label>
            <div class="form-group">
                <input type="text" name="nip" value="<?=$nip?>" class="form-control" placeholder="Masukkan Username" required>
            </div>
            <label>Nama</label>
            <div class="form-group">
                <input type="text" value="<?=$name?>" name="name" class="form-control" placeholder="Masukkan Nama" required>
            </div>
            <label>Password</label>
            <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="Masukkan Update Password">
            </div>
            <div class="form-group">
                <label>Jenis Kelamin</label>
                <select name="gender" class="form-control">
                    <option value="" disabled selected>Pilih Jenis Kelamin</option>
                    <option value="L" <?php echo ($gender == "L")?'selected="selected"':''?>>Laki - laki</option>
                    <option value="P" <?php echo ($gender == "P")?'selected="selected"':''?>>Perempuan</option>
                </select>
            </div>
            <label>Jabatan</label>
            <div class="form-group">
                <input type="text" value="<?=$position?>" name="position" class="form-control" placeholder="Masukkan Jabatan" required>
            </div>
            <div class="form-group">
                <label>Role User</label>
                <select name="role_user" class="form-control user_role">
                    <option value="" disabled selected>Pilih Role User</option>
                    <option value="001" <?php echo ($role_user == "001")?'selected="selected"':''?>>Admin</option>
                    <option value="002" <?php echo ($role_user == "002")?'selected="selected"':''?>>Management</option>
                    <option value="003" <?php echo ($role_user == "003")?'selected="selected"':''?>>Project Manager</option>
                    <option value="004" <?php echo ($role_user == "004")?'selected="selected"':''?>>Staff Project Manager</option>
                    <option value="005" <?php echo ($role_user == "005")?'selected="selected"':''?>>Staff Divisi Keproyekan</option>
                </select>
            </div>
            <div class="modal-footer">
                <button class="btn btn-info" type="submit">Simpan</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>
</div>