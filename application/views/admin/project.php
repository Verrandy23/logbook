
<div class="col-md-12 col-sm-12 col-xs-12">
    <h4 class="text-center">Data project </h4>
    <hr>
</div>
<div class="table-responsive">
<?php if($role_user == 005 || $role_user == 002 || $role_user == 005 || $role_user == 001) { ?>
    <p>
        <a style="margin-left: 1%" href="<?php echo site_url('project/create_project')?>"><button class="btn btn-primary"><i class="fa fa-plus"></i> Input Data </button></a>
    </p>
<?php } else {?>  
<?php }?>

<?php $this->view('messages') ?>
    <table id="tableData" class="table table-striped jambo_table bulk_action">
        <thead>
            <tr>
                <th>No</th>
                <th>Project</th>
                <th>Project Code</th>
                <th>Description</th>
                <th>WPS Project</th>
                <?php if($role_user != 001) { ?>
                <?php } else {?>  
                <th>Aksi</th>
                <?php }?>

            </tr>
        </thead>
        <tbody>
            <?php 
                foreach ($project as $index => $row) {                  
                  ?>
            <tr class="odd gradeX">
                <td><?= $index+1?></td>
                <td><?= $row->name;?></td>
                <td><?= $row->project_code ;?></td>
                <td><?= $row->description ;?></td>
                <td><?= $row->project_parent ;?></td>
                <?php if($role_user != 001) { ?>
                <?php } else {?>  

                <td><a href="<?php echo site_url('project/edit_project/'.$row->project_id); ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a><br><br>
                    <a href="<?php echo site_url('project/delete_project/'.$row->project_id); ?>" class="btn btn-danger" onClick="return confirm('Yakin ingin menghapus Data ini?')"><i class="fa fa-trash"></i></a>
                </td>
                <?php }?>

            </tr>
            <?php
                }
                ?>
        </tbody>
    </table>
</div>
</body>
</html>