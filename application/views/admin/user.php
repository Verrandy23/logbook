
<div class="col-md-12 col-sm-12 col-xs-12">
    <h4 class="text-center">Data User</h4>
    <hr>
</div>
<div class="table-responsive">
    <?php if($role_user != 001) { ?>
    <?php } else {?> 
    <p>
        <a style="margin-left: 1%" href="<?php echo site_url('user/create_user')?>"><button class="btn btn-primary"><i class="fa fa-plus"></i> Input Data </button></a>
    </p>
    <?php } ?>

    <?php $this->view('messages') ?>

    <table id="tableData" class="table table-striped jambo_table bulk_action">
        <thead>
            <tr>
                <th>No</th>
                <th>NIP</th>
                <th>Nama</th>
                <th>Jenis Kelamin</th>
                <th>Jabatan</th>
                <th>Project To PM</th>
                <th class="text-center">Role User</th>
                <?php if($role_user != 001) { ?>
                <?php } else {?> 
                <th>Aksi</th>
                <?php } ?>
            </tr>
        </thead>
        <tbody>
            <?php 
                foreach ($user as $index => $row) {                  
                  ?>
            <tr class="odd gradeX">
                <td><?= $index+1?></td>
                <td><?= $row->nip;?></td>
                <td><?= $row->name;?></td>
                <td>
                    <?php 
                        if($row->gender == "L"){
                         echo "Laki - laki";
                        }
                        else if($row->gender == "P"){
                         echo "Perempuan";
                        }
                        ?>
                </td>
                <td><?= $row->position ;?></td>
                <td>
                    <?php 
                        if($row->project_to_pm == null){
                         echo "Keproyekan";
                        }
                        else{
                         echo $row->project_name;
                        }
                        ?>
                </td>
                <td class="text-center">
                    <?php 
                        if($row->role_user == 002){
                         echo "Management";
                        }
                        else if($row->role_user == 003){
                         echo "Project Manager";
                        } else if($row->role_user == 004){
                         echo "Staff Project Manager";
                        } else if($row->role_user == 005){
                         echo "Staff Divisi Keproyekan";
                        }
                        ?>
                </td>
                <?php if($role_user != 001) { ?>
                <?php } else {?> 
                <td><a href="<?php echo site_url('user/edit_user/'.$row->id); ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a><br><br>
                    <a href="<?php echo site_url('user/delete_user/'.$row->id); ?>" class="btn btn-danger" onClick="return confirm('Yakin ingin menghapus Data ini?')"><i class="fa fa-trash"></i></a>
                </td>
                <?php } ?>

            </tr>
            <?php
                }
                ?>
        </tbody>
    </table>
</div>
</body>
</html>