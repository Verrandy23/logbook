<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_user extends CI_Model
{
   
    function get_user()
    {
        $sql = "SELECT users.*,projects.name as project_name from users 
        LEFT JOIN projects ON users.project_to_pm = projects.id ORDER BY users.id DESC";
        $query = $this->db->query($sql);
        return $query->result();  
    } 
  

    public function insert_db_user($data)
    {
        $this->db->insert('users',$data);
    }
  

     function get_userid($id)
    {
      $query  =   $this->db->where('id', $id);
      $query  =   $this->db->get('users');
      return $query->row();
    }

    function update_user($param,$id)
    {
        $this->db->where('id',$id);
        $this->db->update('users',$param);
        if($this->db->affected_rows()>0)
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    function delete_user($id)
    {
        $this->db->delete('users',array('id' =>$id ));
        return;
    }



    
}