<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_issue extends CI_Model
{
   
    function get_issue()
    {
    $sql = "select DISTINCT(issues.name), issues.action, issues.id,issues.status, issues.pic,issues.issue,issues.level, issues.name as pic_name,projects.name as project_name ,finished.name as finished_name, category_issues.name_category as category from issues join users as pic on issues.pic = pic.id left join users as finished on issues.finished_by = finished.id LEFT JOIN projects ON issues.project_id = projects.id LEFT JOIN category_issues ON issues.category_id=category_issues.id_category_issues ORDER BY issues.id ASC ";
        $query = $this->db->query($sql);
        return $query->result();  
    } 
  

    public function insert_db_issue($data)
    {
        $this->db->insert('issues',$data);
    }
  

    function get_issueid($id)
    {
      $query  =   $this->db->where('id', $id);
      $query  =   $this->db->get('issues');
      return $query->row();
    }

    function update_issue($param,$id)
    {
        $this->db->where('id',$id);
        $this->db->update('issues',$param);
        if($this->db->affected_rows()>0)
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    function delete_issue($id)
        {
        $this->db->delete('issues',array('id' =>$id ));
        return;
    }
    
    function get_open_issue(){
        $open = $this->db->where('status','open')->from('issues')->count_all_results();
        return $open;
    }

    function get_close_issue(){
        $close = $this->db->where('status','close')->from('issues')->count_all_results();
        return $close;
    }
    function get_category_issue(){

        $sql = 'select * from category_issues';
        $query = $this->db->query($sql);

        return $query->result();
    }

     
}