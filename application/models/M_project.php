<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_project extends CI_Model
{
   
    function get_projects()
    {
        $sql = "SELECT projects.id as project_id,projects.name,master_projects.* from projects 
        LEFT JOIN master_projects ON projects.master_project_id = master_projects.id 
        ORDER BY projects.id DESC";
        $query = $this->db->query($sql);
        return $query->result();  
    }
    
    function get_master_projects()
    {
        $sql = "SELECT * from master_projects";
        $query = $this->db->query($sql);
        return $query->result();  
    }
  

    public function insert_db_project($data)
    {
        $this->db->insert('projects',$data);
    }
  

     function get_projectid($id)
    {
      $query  =   $this->db->where('id', $id);
      $query  =   $this->db->get('projects');
      return $query->row();
    }

    function update_project($param,$id)
    {
        $this->db->where('id',$id);
        $this->db->update('projects',$param);
        if($this->db->affected_rows()>0)
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    function delete_project($id)
    {
        $this->db->delete('projects',array('id' =>$id ));
        return;
    }
 
    function project(){

        $pic = $this->session->userdata('pic');
        $sql = "SELECT projects.name as project FROM projects INNER JOIN users on users.project_to_pm = projects.id where users.id = $pic";
        $query = $this->db->query($sql)->row();

        return $query;

    }

    function total(){

        $pic = $this->session->userdata('pic');
        $sql = "SELECT COUNT(*) as total FROM activity_project_plans INNER JOIN projects ON activity_project_plans.project_id = projects.id where activity_project_plans.pic = $pic";
        
        $query = $this->db->query($sql)->row();

        return $query;
    }
    function get_anggota(){

        $pic = $this->session->userdata('pic');
        $sql = "select count(*) as total from users where users.project_to_pm in (SELECT users.project_to_pm from users WHERE id = $pic) ";
        $query = $this->db->query($sql)->row();
       
        return $query;
    
    }


}