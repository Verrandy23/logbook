<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_activity_plan extends CI_Model
{
   
    function get_activity()
    {
        $sql = "SELECT activity_project_plans.*,users.name as user_name,projects.name as project_name 
        from activity_project_plans JOIN projects ON activity_project_plans.project_id = projects.id 
        JOIN users ON activity_project_plans.pic = users.id  ORDER BY activity_project_plans.id DESC";
        $query = $this->db->query($sql);
        return $query->result();  
    } 
  

    public function insert_db_activity($data)
    {
        $this->db->insert('activity_project_plans',$data);
    }
  

     function get_activityid($id)
    {
      $query  =   $this->db->where('id', $id);
      $query  =   $this->db->get('activity_project_plans');
      return $query->row();
    }

    function update_activity($param,$id)
    {
        $this->db->where('id',$id);
        $this->db->update('activity_project_plans',$param);
        if($this->db->affected_rows()>0)
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    function delete_activity($id)
    {
        $this->db->delete('activity_project_plans',array('id' =>$id ));
        return;
    }


}