<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_activity_project extends CI_Model
{
   
    function get_record()
    {
      $sql = "SELECT activity_projects.*,categories.name as category_name,users.name as user_name,projects.name as project_name 
            FROM activity_projects JOIN users ON activity_projects.pic = users.id JOIN projects 
            ON activity_projects.project_id = projects.id join categories ON activity_projects.category_id = categories.id 
            ORDER BY activity_projects.id DESC";
            $query = $this->db->query($sql);
            return $query->result();  
    } 
    function get_project()
    {
      $sql = "SELECT * FROM projects";
            $query = $this->db->query($sql);
            return $query->result();  
    } 

     function get_issue()
    {
      $sql = "SELECT * FROM issues JOIN projects ON issues.project_id = projects.id";
            $query = $this->db->query($sql);
            return $query->result();  
    } 

    function get_total_project()
    {
      $sql = "SELECT COUNT(*) as total FROM projects";
            $query = $this->db->query($sql);
            return $query->result();  
    } 

    function get_total_activity()
    {
      $sql = "SELECT COUNT(*) as total FROM activity_projects";
            $query = $this->db->query($sql);
            return $query->result();  
    } 

    function get_total_attachment()
    {
      $sql = "SELECT COUNT(*) as total FROM attachments";
            $query = $this->db->query($sql);
            return $query->result();  
    } 

    function get_total_issue()
    {
      $sql = "SELECT COUNT(*) as total FROM issues";
            $query = $this->db->query($sql);
            return $query->result();  
    } 

    function get_total_plan()
    {
      $sql = "SELECT COUNT(*) as total FROM activity_project_plans";
            $query = $this->db->query($sql);
            return $query->result();  
    } 

    function get_total_user()
    {
      $sql = "SELECT COUNT(*) as total FROM users";
            $query = $this->db->query($sql);
            return $query->result();  
    } 

    function get_month()
    {
      $sql = "SELECT MONTHNAME(start_date) as month,COUNT(start_date) as jumlah FROM activity_projects WHERE start_date LIKE '%2019%' GROUP BY MONTH(start_date)";
            $query = $this->db->query($sql);
            return $query->result();  
    } 

    function get_month_new()
    {
      $sql = "SELECT MONTHNAME(start_date) as month,COUNT(start_date) as jumlah FROM activity_projects WHERE start_date LIKE '%2020%' GROUP BY MONTH(start_date)";
            $query = $this->db->query($sql);
            return $query->result();  
    } 

    function get_category()
    {
      $sql = "SELECT * FROM categories";
            $query = $this->db->query($sql);
            return $query->result();  
    } 

    public function insert_db_record($data)
    {
        $this->db->insert('activity_projects',$data);
    }
    public function insert_attachment_file($data)
    {
        $this->db->insert('attachments',$data);
    }

     function get_id($id)
    {
      $query  =   $this->db->where('id', $id);
      $query  =   $this->db->get('activity_projects');
      return $query->row();
    }
    function update_data($param,$id)
    {
        $this->db->where('id',$id);
        $this->db->update('activity_projects',$param);
        if($this->db->affected_rows()>0)
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    function delete_data($id)
    {
        $this->db->delete('activity_projects',array('id' =>$id ));
        return;
    }

    function delete_data_attachment($id)
    {
        $this->db->delete('attachments',array('attachmentid' =>$id ));
        return;
    }

    function get_attachment($id)
    {
      $sql = "SELECT attachments.id,activity_project_id,attachment, 
            SUBSTRING_INDEX(attachment,'.',-1) AS format,activity_projects.pic from attachments 
            JOIN activity_projects ON attachments.activity_project_id = activity_projects.id 
            WHERE activity_project_id=$id";
            $query = $this->db->query($sql);
            return $query->result();  
    } 


    
}