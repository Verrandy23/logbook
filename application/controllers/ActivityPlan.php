<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ActivityPlan extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('m_activity_project');
        $this->load->model('m_activity_plan');

        if($this->session->userdata('status') != "success_login"){
            redirect('login/');
        }
    }

/* Manage Activity Plan */
  public function index()
  {
    $data['pic']            = $this->session->userdata('pic');
    $data['project']        = $this->m_activity_project->get_project();
    $data['activity']       = $this->m_activity_plan->get_activity();
    $data['main_content']   =  'admin/activity';
    $this->load->view('admin/template',$data); 
  }



  public function create_activity(){
    $data['pic']            = $this->session->userdata('pic');
    $data['project']        = $this->m_activity_project->get_project();
    $data['main_content']   =  'admin/create_activity';
    $this->load->view('admin/template',$data); 
  }

  public function insert_data_activity()
  {

    $pic = $this->session->userdata('pic');
    $data = array(
                  'name'                    => $this->input->post('name'),
                  'plan_start_date'         => $this->input->post('plan_start_date'),
                  'plan_end_date'           => $this->input->post('plan_end_date'),
                  'created_at'              => $this->input->post('created_at'),
                  'description'             => $this->input->post('description'),
                  'status_realization'      => $this->input->post('status_realization'),
                  'note'                    => $this->input->post('note'),
                  'pic'                     => $pic,
                  'project_id'              => $this->input->post('projectid'),

              );
    $this->session->set_flashdata('success', 'Data berhasil ditambahkan');

    $this->m_activity_plan->insert_db_activity($data);
    redirect('activityplan'); 
  }


  public function edit_activity()
  {

    $id = $this->uri->segment(3);
    $data['project']                = $this->m_activity_project->get_project();
    $row                            = $this->m_activity_plan->get_activityid($id);
    $data['id']                     = $id;
    $data['name']                   = $row->name;
    $data['plan_start_date']        = $row->plan_start_date;
    $data['plan_end_date']          = $row->plan_end_date;
    $data['created_at']             = $row->created_at;
    $data['description']            = $row->description;
    $data['status_realization']     = $row->status_realization;      
    $data['note']                   = $row->note;      
    $data['project_id']             = $row->project_id;      
    $data['main_content']   = 'admin/edit_activity';
    $this->load->view('admin/template',$data);
  }

  public function update_activity($id)
  {
      $param=array(
          'name'                => $this->input->post('name'),
          'plan_start_date'     => $this->input->post('plan_start_date'),
          'plan_end_date'       => $this->input->post('plan_end_date'),
          'created_at'          => $this->input->post('created_at'),
          'description'         => $this->input->post('description'),
          'status_realization'  => $this->input->post('status_realization'),
          'note'                => $this->input->post('note'),
          'project_id'          => $this->input->post('projectid'),

      );
      $this->session->set_flashdata('update', 'Data berhasil diupdate');
      $proses = $this->m_activity_plan->update_activity($param,$id);
      redirect('activityplan');
  }

  public function delete_activity()
  {
      $id   = $this->uri->segment(3);
      $this->m_activity_plan->delete_activity($id);

      $this->session->set_flashdata('delete', 'Data telah dihapus');
      redirect('activityplan');
  }
}