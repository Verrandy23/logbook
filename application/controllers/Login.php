<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	 public function __construct()
    {
        parent::__construct();
        $this->load->library('session');


    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('admin/login');
	}

	public function login(){
	  $nip = $this->input->post('nip');
      $password = md5($this->input->post('password'));
      $this->db->where('nip', $nip);
      $this->db->where('password', $password);
      $get_data = $this->db->get('users')->row();
  
      if (!empty($get_data)) {
        $session_data = array(
            'role_user' 	=> 	$get_data->role_user,
            'nip' 			=> 	$get_data->name,
            'project_to_pm' => 	$get_data->project_to_pm,
            'pic' 			=>	$get_data->id,
            'status' 		=> 'success_login'
            );
        $this->session->set_userdata($session_data);
        redirect('dashboard/');

      }else{
          
          echo '<script> alert("nip password salah") </script>';
          $this->load->view('admin/login');

      }
	}
	function logout(){
		$this->session->sess_destroy();
		redirect(site_url('login/'));
	}
}
