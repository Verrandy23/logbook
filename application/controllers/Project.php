<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('m_project');
        $this->load->model('m_user');

        if($this->session->userdata('status') != "success_login"){
            redirect('login/');
        }
    }

  /* Manage Project */
  public function index()
  {
    $data['role_user']      = $this->session->userdata('role_user');
    $data['project']        = $this->m_project->get_projects();
    $data['main_content']   =  'admin/project';
    $this->load->view('admin/template',$data); 
  }



  public function create_project(){
    $data['pic'] = $this->session->userdata('pic');
    $data['master_project'] = $this->m_project->get_master_projects();
    $data['main_content']   =  'admin/create_project';
    $this->load->view('admin/template',$data); 
  }

  public function insert_data_project()
  {

    $pic = $this->session->userdata('pic');
    $data = array(
                  'name'                => $this->input->post('name'),
                  'master_project_id'   => $this->input->post('master_project_id'),
             

              );
    $this->session->set_flashdata('success', 'Data berhasil ditambahkan');
    $this->m_project->insert_db_project($data);
    redirect('project'); 
  }


  public function edit_project()
  {

    $id = $this->uri->segment(3);
    $data['master_project']    = $this->m_project->get_master_projects();
    $row                       = $this->m_project->get_projectid($id);
    $data['id']                = $id;
    $data['name']              = $row->name;
    $data['master_project_id'] = $row->master_project_id;      
    $data['main_content']   = 'admin/edit_project';
    $this->load->view('admin/template',$data);
  }

  public function update_project($id)
  {
      $param=array(
          'name'                => $this->input->post('name'),
          'master_project_id'   => $this->input->post('master_project_id'),
      );

      $this->session->set_flashdata('update', 'Data berhasil diupdate');

      $proses = $this->m_project->update_project($param,$id);
      redirect('project');
  }

  public function delete_project()
  {
      $id   = $this->uri->segment(3);
      $this->m_project->delete_project($id);

      $this->session->set_flashdata('delete', 'Data telah dihapus');
      redirect('project');
  }

    // function get_master_project()
    // {
    //   $sql = "SELECT * FROM projects JOIN projects ON projects.project_id = projects.id";
    //   $query = $this->db->query($sql);
    //   return $query->result();  
    // } 

}