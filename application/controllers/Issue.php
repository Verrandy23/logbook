<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Issue extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('m_activity_project');
        $this->load->model('m_issue');

        if($this->session->userdata('status') != "success_login"){
            redirect('login/');
        }
    }

/* Manage Issue */
  public function index()
  {
    $data['pic'] = $this->session->userdata('pic');
    $data['issue'] = $this->m_issue->get_issue();

    $data['main_content']   =  'admin/issue';
    $this->load->view('admin/template',$data); 
  }


  public function create_issue(){
    $data['pic'] = $this->session->userdata('pic');
    $data['category']=$this->m_issue->get_category_issue();
    $data['project'] = $this->m_activity_project->get_project();
    $data['main_content']   =  'admin/create_issue';
    $this->load->view('admin/template',$data); 
  }

  public function insert_data_issue()
  {

    $pic = $this->session->userdata('pic');
    $data = array(

                  'name'        => $this->input->post('name'),
                  'issue'       => $this->input->post('issue'),
                  'action'      => $this->input->post('action'),
                  'level'       => $this->input->post('level'),
                  'status'      => $this->input->post('status'),
                  'project_id'  => $this->input->post('projectid'),
                  'pic'         => $pic,
                  'category_id'   => $this->input->post('cat_issue')

              );
    $this->session->set_flashdata('success', 'Data berhasil ditambahkan');

    $this->m_issue->insert_db_issue($data);
    redirect('issue'); 
  }


  public function edit_issue()
  {

    $id = $this->uri->segment(3);
    $data['project']        = $this->m_activity_project->get_project();
    $row                    = $this->m_issue->get_issueid($id);
    $data['id']             = $id;
    $data['name']           = $row->name;
    $data['issue']          = $row->issue;
    $data['action']         = $row->action;
    $data['level']          = $row->level;
    $data['status']         = $row->status;
    $data['project_id']     = $row->project_id;      
    $data['finished_by']    = $row->finished_by;      
    $data['main_content']   = 'admin/edit_issue';
    $this->load->view('admin/template',$data);
  }

  public function update_issue($id)
  {
      $param=array(
          'project_id'      => $this->input->post('project'),
          'name'            => $this->input->post('name'),
          'issue'           => $this->input->post('issue'),
          'action'          => $this->input->post('action'),
          'level'           => $this->input->post('level'),
          'status'          => $this->input->post('status'),
          'finished_by'     => $this->input->post('finished_by'),
      );

      $this->session->set_flashdata('update', 'Data berhasil diupdate');
      $proses = $this->m_issue->update_issue($param,$id);
      redirect('issue');
  }

  public function delete_issue()
  {
      $id   = $this->uri->segment(3);
      $this->m_issue->delete_issue($id);

      $this->session->set_flashdata('delete', 'Data telah dihapus');
      redirect('issue');
  }
  public function close_issue($id)
  {

  }
}