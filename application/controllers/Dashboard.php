<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('m_activity_project');
        $this->load->model('m_issue');
        $this->load->model('m_project');
    
        if($this->session->userdata('status') != "success_login"){
            redirect('login/');
        }
    }

public function index()
  {
   
    $data['total_project']        = $this->m_activity_project->get_total_project(); 
    $data['total_activity']       = $this->m_activity_project->get_total_activity(); 
    $data['total_attachment']     = $this->m_activity_project->get_total_attachment(); 
    $data['total_issue']          = $this->m_activity_project->get_total_issue(); 
    $data['total_activity_plan']  = $this->m_activity_project->get_total_plan(); 
    $data['total_user']           = $this->m_activity_project->get_total_user(); 
    $data['dashboard']            = $this->m_activity_project->get_month(); 
    $data['dashboard_new']        = $this->m_activity_project->get_month_new(); 
    $data['dashboard_open_issue'] = $this->m_issue->get_open_issue();
    $data['dashboard_close_issue']= $this->m_issue->get_close_issue();
    $data['project']              = $this->m_project->project();
    $data['user']                 = $this->m_project->total();
    $data['get_anggota']          = $this->m_project->get_anggota();  
    $data['role_user']            = $this->session->userdata('role_user');
    $data['main_content']         =  'admin/dashboard';
    $this->load->view('admin/template',$data);   
  }
}