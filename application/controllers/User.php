<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('m_activity_project');
        $this->load->model('m_issue');
        $this->load->model('m_user');

        if($this->session->userdata('status') != "success_login"){
            redirect('login/');
        }
    }

/* Manage User */
  public function index()
  {
    $data['user']           = $this->m_user->get_user();
    $data['role_user']      = $this->session->userdata('role_user');
    $data['main_content']   =  'admin/user';
    $this->load->view('admin/template',$data); 
  }

  public function get_user_role()
  {
    $data=$this->input->post('id');
    echo json_encode($data);
  }


  public function create_user(){
    $data['project'] = $this->m_activity_project->get_project();
    $data['main_content']   =  'admin/create_user';
    $this->load->view('admin/template',$data); 
  }

  public function insert_data_user()
  {

    $data = array(
                  'nip'             => $this->input->post('nip'),
                  'name'            => $this->input->post('name'),
                  'password'        => md5($this->input->post('password')),
                  'gender'          => $this->input->post('gender'),
                  'position'        => $this->input->post('position'),
                  'project_to_pm'   => $this->input->post('project_to_pm'),
                  'role_user'       => $this->input->post('role_user'),

              );

    $this->session->set_flashdata('success', 'Data berhasil ditambahkan');
    $this->m_user->insert_db_user($data);    
    redirect('user'); 
  }


  public function edit_user()
  {

    $id = $this->uri->segment(3);
    $data['project']        = $this->m_activity_project->get_project();
    $row                    = $this->m_user->get_userid($id);
    $data['id']             = $id;
    $data['nip']            = $row->nip;
    $data['name']           = $row->name;
    $data['gender']         = $row->gender;
    $data['position']       = $row->position;
    $data['project_to_pm']  = $row->project_to_pm;
    $data['role_user']      = $row->role_user;      
    $data['main_content']   = 'admin/edit_user';
    $this->load->view('admin/template',$data);
  }

  public function update_user($id)
  {
    if(empty($this->input->post('password'))){
      $param=array(
          'nip'             => $this->input->post('nip'),
          'name'            => $this->input->post('name'),
          'gender'          => $this->input->post('gender'),
          'position'        => $this->input->post('position'),
          'project_to_pm'   => $this->input->post('project_to_pm'),
          'role_user'       => $this->input->post('role_user'),
      );
    } else {
       $param=array(
          'nip'             => $this->input->post('nip'),
          'password'        => md5($this->input->post('password')),
          'name'            => $this->input->post('name'),
          'gender'          => $this->input->post('gender'),
          'position'        => $this->input->post('position'),
          'project_to_pm'   => $this->input->post('project_to_pm'),
          'role_user'       => $this->input->post('role_user'),
      );
    }

      $this->session->set_flashdata('update', 'Data berhasil diupdate');

      $proses = $this->m_user->update_user($param,$id);
      redirect('user');
  }
  
  public function delete_user()
  {
      $id   = $this->uri->segment(3);
      $this->m_user->delete_user($id);

      $this->session->set_flashdata('delete', 'Data telah dihapus');

      redirect('user');
  }


}
