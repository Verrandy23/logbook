<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ActivityProject extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('m_activity_project');
        $this->load->model('m_activity_plan');
        $this->load->model('m_issue');
        $this->load->model('m_user');


        if($this->session->userdata('status') != "success_login"){
            redirect('login/');
        }
    }

	public function index()
	{
		$data['pic']            = $this->session->userdata('pic');
		$data['record']         = $this->m_activity_project->get_record();
		$data['project']        = $this->m_activity_project->get_project();
		$data['category']       = $this->m_activity_project->get_category();
		$data['main_content']   =  'admin/beranda';
    $this->load->view('admin/template',$data); 
	}


  public function dashboard()
  {
   
    $data['total_project']        = $this->m_activity_project->get_total_project(); 
    $data['total_activity']       = $this->m_activity_project->get_total_activity(); 
    $data['total_attachment']     = $this->m_activity_project->get_total_attachment(); 
    $data['total_issue']          = $this->m_activity_project->get_total_issue(); 
    $data['total_activity_plan']  = $this->m_activity_project->get_total_plan(); 
    $data['total_user']           = $this->m_activity_project->get_total_user(); 
    $data['dashboard']            = $this->m_activity_project->get_month(); 
    $data['dashboard_new']        = $this->m_activity_project->get_month_new(); 
    $data['main_content']         =  'admin/dashboard';
    $this->load->view('admin/template',$data);   
  }

  public function create(){
    $data['pic']            = $this->session->userdata('pic');
    $data['record']         = $this->m_activity_project->get_record();
    $data['project']        = $this->m_activity_project->get_project();
    $data['category']       = $this->m_activity_project->get_category();
    $data['main_content']   =  'admin/create';
    $this->load->view('admin/template',$data); 
  }

	public function insert_data()
	{
	    date_default_timezone_set("Asia/Jakarta");
        $created_at = date("Y-m-d");
		$pic = $this->session->userdata('pic');
		$data = array(
                    'project_id'        => $this->input->post('project'),
                    'category_id'       => $this->input->post('category'),
                    'start_date'        => $this->input->post('start_date'),
                    'end_date'          => $this->input->post('end_date'),
                    'name_activity'     => $this->input->post('name_activity'),
                    'discussion'        => $this->input->post('discussion'),
                    'action'            => $this->input->post('action'),
                    'information'       => $this->input->post('information') ,
                    'pic'               => $pic,
                    'created_at'        => $created_at,

                );
        $this->session->set_flashdata('success', 'Data berhasil ditambahkan');
        $this->m_activity_project->insert_db_record($data);
        redirect('activityproject'); 
	}


    public function edit()
    {
        $id = $this->uri->segment(3);
        $data['project']        = $this->m_activity_project->get_project();
        $data['category']       = $this->m_activity_project->get_category();
        $row                    = $this->m_activity_project->get_id($id);
        $data['id']             = $id;
        $data['name_activity']  = $row->name_activity;
        $data['start_date']     = $row->start_date;
        $data['end_date']       = $row->end_date;
        $data['discussion']     = $row->discussion;
        $data['action']         = $row->action;
        $data['information']    = $row->information;
        $data['project_id']     = $row->project_id;
        $data['category_id']    = $row->category_id;
        $data['main_content']   = 'admin/edit';
        $this->load->view('admin/template',$data);
  }

  public function update($id)
  {
    $param=array(
        'project_id'    => $this->input->post('project'),
        'category_id'   => $this->input->post('category'),
        'start_date'    => $this->input->post('start_date'),
        'end_date'      => $this->input->post('end_date'),
        'name_activity' => $this->input->post('name_activity'),
        'discussion'    => $this->input->post('discussion'),
        'action'        => $this->input->post('action'),
        'information'   => $this->input->post('information') 
    );

    $this->session->set_flashdata('update', 'Data berhasil diupdate');
    $proses = $this->m_activity_project->update_data($param,$id);
    redirect('activityproject');
  }

  public function delete()
  {
    $id   = $this->uri->segment(3);
    $this->m_activity_project->delete_data($id);

    $this->session->set_flashdata('delete', 'Data telah dihapus');
    redirect('activityproject');
  }

  public function attachment(){
    $id                 = $this->uri->segment(3);
    $data['id']         = $id;
    $data['attachment'] = $this->m_activity_project->get_attachment($id);
    $data['main_content']   = 'admin/attachment';
    $this->load->view('admin/template',$data);
 

}
public function vd_attachment(){
    $ftp_username ="devapp";
    $ftp_userpass = "usrdevftp28";
    $ftp_server = "10.10.0.212";
    $params  = $this->uri->segment(3);
    $urllink = urldecode($params);
    $ftp_conn = ftp_ssl_connect($ftp_server) or die("Could not connect to $ftp_server");
    $login = ftp_login($ftp_conn, $ftp_username, $ftp_userpass);
    ftp_pasv($ftp_conn, true) or die("Cannot switch to passive mode");

    if (!file_exists('tmp')) {
      mkdir('tmp', 0755, true);
    }

    $local_file = 'tmp/'.$urllink;
    $server_file = '/ftp01/dev/logbook_attachment/ftp_uploads/'.$urllink;


    // download server file
    if (ftp_get($ftp_conn, $local_file, $server_file, FTP_BINARY))
      {
        if(strpos($urllink,".pdf")) {
            header('Content-type: application/pdf');
        }
        else if(strpos($urllink,".png")){
            header('Content-type:image/png');
           
        }
        else if(strpos($urllink,".jpg")){
            header('Content-type: image/jpeg');
           
        }
        else if(strpos($urllink,".jpeg")){
            header('Content-type: image/jpeg');
            
        }
         else if(strpos($urllink,".docx")){
            header('Content-type:  application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        }
        header('Content-Transfer-Encoding: binary'); 
        header('Accept-Ranges: bytes'); 
        // Read the file 
        @readfile($local_file); 
      }
    else
      {
        echo '<script> alert("Data attachment tidak ada") </script>';
        redirect('admin');
      }

    // close connection

    ftp_close($ftp_conn);

}
 public function vu_attachment(){
  $ftp_username ="devapp";
  $ftp_userpass = "usrdevftp28";
  $ftp_server = "10.10.0.212";
  $name = 'ftp01/dev/logbook_attachment/ftp_uploads/'.$_FILES['file']['name'];
  $file = $_FILES['file']['tmp_name'];
  $ftp_conn = ftp_ssl_connect($ftp_server) or die("Could not connect to $ftp_server");
  $login = ftp_login($ftp_conn, $ftp_username, $ftp_userpass);
  ftp_pasv($ftp_conn, true) or die("Cannot switch to passive mode");

  if (ftp_put($ftp_conn,$name,$file,FTP_BINARY))
    {
    echo "Successfully upload $file.";
    }
  else
    {
    echo "Error upload $file.";
    }


  ftp_close($ftp_conn);

  $data = $_FILES['file']['name'];
  $id_activity_project =  $this->input->post('id');

   $data = array(
                'attachment'            => $data,
                'activity_project_id'   => $id_activity_project,

            );
        $this->m_activity_project->insert_attachment_file($data);
        redirect('activityproject/attachment/'.$id_activity_project); 


 }

 public function delete_attachment(){
    $ftp_username ="devapp";
    $ftp_userpass = "usrdevftp28";
    $ftp_server = "10.10.0.212";
    $params  = $this->uri->segment(3);
    $urllink = urldecode($params);
    $ftp_conn = ftp_ssl_connect($ftp_server) or die("Could not connect to $ftp_server");
    $login = ftp_login($ftp_conn, $ftp_username, $ftp_userpass);
    ftp_pasv($ftp_conn, true) or die("Cannot switch to passive mode");

    $file = 'ftp01/dev/logbook_attachment/ftp_uploads/'.$params;

  // try to delete file
  if (ftp_delete($ftp_conn, $file))
    {
    echo "$file deleted";
    }
  else
    {
    echo "Could not delete $file";
    }

  // close connection
    ftp_close($ftp_conn);
    $id   = $this->uri->segment(4);
    $this->m_activity_project->delete_data_attachment($id);
    redirect('activityproject');

    }

}