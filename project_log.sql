-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 13, 2020 at 03:27 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project_log`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbactivityplan`
--

CREATE TABLE `tbactivityplan` (
  `activityplanid` int(11) NOT NULL,
  `aktivitas` varchar(100) NOT NULL,
  `tglrencanaaktivitasstart` varchar(50) NOT NULL,
  `tglrencanaaktivitasend` varchar(50) NOT NULL,
  `tglcreate` varchar(50) NOT NULL,
  `deskripsi` text NOT NULL,
  `statusrealisasi` varchar(50) NOT NULL,
  `catatan` varchar(100) NOT NULL,
  `pic` int(11) NOT NULL,
  `projectid_fk` int(11) NOT NULL,
  `statustingkatprioritas` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbactivityplan`
--

INSERT INTO `tbactivityplan` (`activityplanid`, `aktivitas`, `tglrencanaaktivitasstart`, `tglrencanaaktivitasend`, `tglcreate`, `deskripsi`, `statusrealisasi`, `catatan`, `pic`, `projectid_fk`, `statustingkatprioritas`) VALUES
(10, 'Membuat schedule percepatan LRT untuk TS2 -TS10', '2019-09-30', '2019-09-30', '2019-09-27', 'Schedule percepatan', 'Rencana', 'Sebagai acusn kerja IMS untuk delivery komponen', 10, 8, 'urgent'),
(12, 'aktivitas', '2019-10-01', '2019-10-08', '2019-10-01', 'Aktivitas 1', 'Rencana', 'Catatan ', 2, 12, 'normal'),
(13, 'hh', '2019-10-01', '2019-10-01', '2019-10-01', 'hh', 'Rencana', 'hh', 1, 9, 'normal');

-- --------------------------------------------------------

--
-- Table structure for table `tbattachment`
--

CREATE TABLE `tbattachment` (
  `attachmentid` int(11) NOT NULL,
  `recordid_fk` int(11) NOT NULL,
  `attachment` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbattachment`
--

INSERT INTO `tbattachment` (`attachmentid`, `recordid_fk`, `attachment`) VALUES
(1, 1, 'LRTJBDB1-Rapat-Koordinasi-Progress-Pekerjaan-LRT-Jabodebek-(4Januari2019).pdf'),
(2, 2, 'LRTJBDB2-MeetingpercepatanpembangunanLRT Jabodebek(7-8 Jan2019).pdf'),
(3, 3, 'LRTJBDB3-FAItractionmotoruntukproyekLRTJabodebek(7-9 Jan2019).pdf'),
(4, 4, 'LRTJBDB4-(KAI)NotulenRapat 9-10Januari2019-RapatPembahasanScheduleProduksiSaranaLRTJabodebek.pdf'),
(5, 5, 'LRTJBDB5-KR.104 I 1-DIV.LRT-2019-15Jan2019-PermohonanPenyesuaianInteriordanDimensiKursiLRTJabodebek.'),
(6, 6, 'LRTJBDB6-Paparanhasilstudidanpaparanstudikelayan(15-16Jan2019).pdf'),
(7, 7, 'LRTJBDB7-(KAI)NotulenRapat16-18Januari 2019-PemantauanProgressProduksiSaranaLRTJabodebek.pdf'),
(8, 8, 'LRTJBDB8-(BPPT)NotulenRapatHighLevelMeeting-16Januari2019JWMarriotSurabaya-HasilKegiatanPerkeretapia'),
(9, 9, 'LRT-JBDB9-DaftarHadirRapatFGDkegiatanperkeretaapian16Januari2019diJWMarriotSurbaya.pdf'),
(10, 10, 'LRT-JBDB10-Notulenrapat17Januari2019-MRBCarbodyM-011LRTJabodebek.pdf'),
(11, 11, 'LRTJBDB11-Notulenrapat17Januari2019-EvaluasiCarbodyLRTJabodebek.pdf'),
(12, 12, 'LRTJBDB12-M-01442201922Januari2019-InformasiJadwalKedatanganSubAssyCarbodyLRTJabodebek.pdf'),
(13, 13, 'LRTJBDB13-KoordinasiteknissaranadanprogresssaranaLRTJabodebek(22-24Jan2019).pdf'),
(14, 14, 'LRTJBDB14-Attendancelist23Jan2019-PengawasprogresLRTJabodebek.pdf'),
(15, 15, 'LRTJBDB15-M-0029430201923Jan2019-EvaluasiCarbodyproyekLRTJabodebek.pdf'),
(16, 16, 'LRTJBDB16-Rapat terkaitLRTJabodebek(23-24Jan2019).pdf'),
(17, 17, 'LRTJBDB17-FAIkomponenairsealprojekLRTJabodebek(23-27Januari2019).pdf'),
(18, 18, 'LRTJBDB18-Attendancelist24Januari2019-PengawasanprogressLRTJabodebek.pdf'),
(19, 19, 'LRTJBDB19-MeetingdenganKAIterkaitLRTJabodebek(29Januari2019).pdf'),
(20, 20, 'LRTJBDB20-RapatKoordinasipercepatanpenyelesaianLRTJabodebek(31Januari2019).pdf'),
(21, 21, 'LRTJBDB21-Attendancelist6Feb2019-PembahasandendaCAFterkaitLRTJabodebek.pdf'),
(22, 22, 'LRTJBDB22-(KAI)NotulenRapat7Februari2019-PembahasanRevisiMasterScheduleLRTJabodebek.pdf'),
(23, 23, 'LRTJBDB23-MeetingdenganDeputyTransportasiPemprovJakarta(14-15Februari2019).pdf'),
(24, 24, 'LRTJBDB24-SurveylokasidanpembahasanrencanakerjaTemporaryPitStop(14-15Februari2019).pdf'),
(25, 25, 'LRTJBDB25-(KAI)NotulenRapat15Februari2019-RapatPembahasanTemporaryPitStopLRTJabodebek.pdf'),
(26, 26, 'LRTJBDB26-RapatKoordinasiPembahasanIDCproyekLRTJabodebek(19-20Februari2019).pdf'),
(27, 27, 'LRTJBDB27-(KAI)NotulenRapat25-26Februari2019-PembahasanSchedulePembayaranSaranaLRTJabodebek.pdf'),
(28, 28, 'LRTJBDB28-Notulenrapat26Februari 2019-MonitoringdanEvaluasiRekomendasiKNKT.pdf'),
(29, 29, 'LRTJBDB29-MeetingterkaitLRTJabodebek(28Februari-1Maret2019).pdf'),
(30, 30, 'LRTJBDB30-NotulenRapat5Maret2019-KoordinasiUjiFatigueBogieFrameLRTJabodebek.pdf'),
(31, 31, 'LRTJBDB31-PreparationmeetingperihalpengujianbogieframedanpembahasansistemproteksipetirLRTJabodebek(5'),
(32, 32, 'LRTJBDB32-(KAI)NotulenRapat6Maret2019-PembahasanProteksiPetirSaranadanPrasaranaLRTJabodebek.pdf'),
(33, 33, 'LRTJBDB33-(KAI)NotulenRapat6Maret2019-PembahasanTeknsiEmergencyHandle,ObstacleDetection,DerailmentDe'),
(34, 34, 'LRTJBDB34-NotulenRapat6Maret2019-PembahasanEN13749LRTJabodebek.pdf'),
(35, 35, 'LRTJBDB35-NotulenMeeting11Maret2019-NCRWheelSetLRTJabodebek.pdf'),
(36, 36, 'LRTJBDB36-MeetingterkaitLRTSumseldanPembahasanITPLRTJabodebek(11-13Maret2019).pdf'),
(37, 37, 'LRTJBDB37-CourtesymeetingdenganMR.ArkaitzmeetingdengandeputyofTransformation(13-14Maret2019).pdf'),
(38, 38, 'LRTJBDB38-NotulenRapat19Maret2019-PembahasanUjiFatigueBogieFrameLRTJabodebek.pdf'),
(39, 39, 'LRTJBDB39-MeetingdenganRotem,MeetingdenganLRTJabodebek (19-21Maret2019).pdf'),
(40, 40, 'LRTJBDB40-MeetingdenganLRTJabodebekdanPameranrailwaydantransportasi(20-21Maret2019).pdf'),
(41, 41, 'LRTJBDB41-NotulenRapat22Maret2019-KoordinasiJunctionBoxElektrikdanDoorEngineLRTJabodebek.pdf'),
(42, 42, 'LRTJBDB42-NotulenRapat25Maret2019-ReviewPersiapanBogieMountingdanPengujianTS1LRTJabodebek.pdf'),
(43, 43, 'LRTJBDB43-MenghadiriundanganmeetingKAIdanMeetingdenganDeputyTranspotasi(25Maret2019).pdf'),
(44, 44, 'LRTJBDB44-RapatteknisdenganPT.KAIdantrainingleadership(25-28maret2019).pdf'),
(45, 45, 'LRTJBDB45-MeetingdenganKAI,ADhiKarya,danKemenkomar(27Maret2019).pdf'),
(46, 46, 'LRTJBDB46-NotulenRapat29Maret2019-INKA-IMSterkaitproyekLRTJabodebek.pdf'),
(47, 47, 'LRTJBDB47-Attendancelist4April2019-ITPLRTJabodebek.pdf'),
(48, 48, 'LRTJBDB48-MeetingwithMr.Sakaueaboutrollingstockgauge(4April2019).pdf'),
(49, 49, 'LRTJBDB49-MeetingtimpendampingJV.StadlerdenganLawyerINKA(8April2019).pdf'),
(50, 50, 'LRTJBDB50-RapatkoordinasiteknissaranaLRTJabodebek(9April2019).pdf'),
(51, 51, 'LRTJBDB51-NotulenRapat10-12April2019-KoordinasiTeknisPekerjaanSignallingdanPekerjaanPengujianLRTJabo'),
(52, 52, 'LRTJBDB52-PembahasanterkaitstasiunlengkungdanstructuregaugeantarasaranadanprasaranaLRTJabodebek(11Ap'),
(53, 53, 'LRTJBDB53-Rapatkomiteauditdengankomisaris,monitoringsimulasiproyekLRT-(11April2019).pdf'),
(54, 54, 'LRTJBDB54-NotulenRapat18April2019-SupplyKomponenLRTJabodebek.pdf'),
(55, 55, 'LRTJBDB55-MeetingdenganRotemdanMeetingdenganDeputyTransportasi(24-25april2019).pdf'),
(56, 56, 'LRTJBDB56-MeetingdenganOCGJoprissterkaitLRTJabodebek(29-30April2019).pdf'),
(57, 57, 'LRTJBDB57-RapatkoordinasiteknisLRTJabodebekdanFGOLRTSumsel(29 April-3Mei2019).pdf'),
(58, 58, 'LRTJBDB58-NotulenRapat2Mei2019-PercepatanpenyelesaianTS1LRTJabodebek.pdf'),
(59, 59, 'LRTJBDB59-(KAI)2-3Mei2019-PembahasanDetailDesainSaranaLRTJabodebek.pdf'),
(60, 60, 'LRTJBDB60-CAFVisitbersamapotentialcustomer(DeputyTransportasiDKIJakarta(6-12Mei2019).pdf'),
(61, 61, 'LRTJBDB61-PenentuanJarakGapCheckrail,widening,danPITpadastabling(9Mei2019).pdf'),
(62, 62, 'LRTJBDB62-(KAI)NotulenRapat10Mei2019-PembahasanTemporaryPitStopLRTJabodebek(2).pdf'),
(63, 63, 'LRTJBDB63-MenghadirirapatterkaitLRTJabodbek(10Mei2019).pdf'),
(64, 64, 'LRTJBDB64-SeminarLRTSumsel,RapatkoordinasiLRTJabodebek(14-15Mei2019).pdf'),
(65, 65, 'LRTJBDB65-MeetingdenganPTAdhikarya,OCGJoprissdanSJKAterkaitLRTJabodebek(21Mei2019).pdf'),
(66, 66, 'LRTJBDB66-NotulenRapat-21mei2019_R_PembahasanLayoutDepoLRTJabodebek.pdf'),
(67, 67, 'LRTJBDB67-NotulenRapat-23mei2019_R_InspeksidanPengawasanProgresProduksiSaranaLRTJabodebek_opt.pdf'),
(68, 68, 'LRTJBDB68-(KAI)NotulenRapat24Mei2019-KickofMeetingaddendumpengadaansarana186carLRTJabodebek.pdf'),
(69, 69, 'LRTJBDB69-(KAI)NotulenRapat27Mei2019-RapatPembahasanTemporaryPitStop.pdf'),
(70, 70, 'LRTJBDB70-MeetingbersamaDirkeudan64DipoLRTJakpro(28Mei2019).pdf'),
(71, 71, 'LRTJBDB71-NotulenRapat12Juni2019-MappingpersiapanpenyelesaianLRTJabodebekdanDMUPhilipina.pdf'),
(72, 72, 'LRTJBDB72-MenghadiriundanganrapatAdhiKaryaLRTJabodebek(25Juni2019).pdf'),
(73, 73, 'LRTJBDB73-MeetingdenganRotemdanMeetingdenganKAI(26-27Juni2019.pdf'),
(74, 74, 'LRTJBDB74-UjibogieLRTJabodebekdanrapatKoordinasiIntegrasisaranadanprasaranav(26-27Juni2019).pdf'),
(75, 75, 'LRTJBDB75-WorkshopDepoLRTJabodebek(1Juli2019).pdf'),
(76, 76, 'LRTJBDB76-KoordinasiPercepatanPembangunanLRTJabodebek(2-4Juli2019).pdf'),
(77, 77, 'LRTJBDB77-RencanaStablingsaranaLRTJabodebek(2Juli2019).pdf'),
(78, 78, 'LRTJBDB78-PengujiankekuatanstrukturbogieframesaranaLRTJabodebek(3-4Juli2019).pdf'),
(79, 79, 'LRTJBDB79-PembahasanprasaranaLRTtestrunsaranaLRT&pengujiandinamikkekuatanstrukturbogieframeLRTJabode'),
(80, 80, 'LRTJBDB80-RapatAddendumLRTJabodebek(16Juli2019).pdf'),
(81, 81, 'LRTJBDB81-RapatDivisiLRTJabodebek(23Juli2019).pdf'),
(82, 82, 'LRTJBDB82-Permohonanuntukmelanjutkanpekerjaanpemasanganelectricalboxsaranalrtjabodebek(22Juli2019).p'),
(83, 83, 'LRTJBDB83-RapataddendumLRTJabodebek&MeetingKemenkomar(23-25Juli2019).pdf'),
(84, 84, 'LRTJBDB84-PembahasanPowersupplyLRTJabodebek&PKStestrunsaranaLRT(25-26Juli2019).pdf'),
(85, 85, 'LRTJBDB85-NotulenRapat-PembahasanstatusPengujianDinamisBogieLRT(26juli2019).pdf'),
(86, 86, 'LRTJBDB86-NotulenRapat-PermasalahanUjiFatigueBogieFrameLRTJabodebek(30juli2019).pdf'),
(87, 87, 'LRTJBDB87-RIS-001-240-INKA-2019-Pembahasanboxip65lrtjabodebek(2agustus2019).pdf'),
(88, 88, 'LRTJBDB88-SItevisitproyekLRTJabodebek(5-7Agustus2019).pdf'),
(89, 89, 'LRTJBDB89-PersiapanFATdenganKAI(6-7Agustus2019).pdf'),
(90, 90, 'LRTJBDB90-PembahasanbiayapenyambunganlistriksaatujisaranaLRTJabodebek&FGDsinkronisasiLRTJabodebek(12'),
(91, 91, 'LRTJBDB91-FGDsinkronisasipenyelenggaraanLRTJabodebek-(14-15Agustus2019).pdf'),
(92, 92, 'LRTJBDB92-KelengkapandokprasyaratFATLRTJabodebek(16Agustus2019).pdf'),
(93, 93, 'LRTJBDB93-MRBCornerSealdanUpperSeat(senin19agustus2019).pdf'),
(94, 94, 'LRTJBDB94-PengendalianTimelineLRTJabodebek(19-20Agustus2019).pdf'),
(95, 95, 'LRTJBDB95-SurveykeDepoLRTJakarta(20agustus2019).pdf'),
(96, 96, 'LRTJBDB96-HasilMRBCCD(21agustus2019).pdf'),
(97, 97, 'LRTJBDB97-PembahasanterkaitLRTJabodebekdenganDirutKAI&AdhiKarya&DirutINKA(21Agustus2019).pdf'),
(98, 98, 'LRTJBDB98-IntroductionscopeISALRTJabodebek(29Agustus2019).pdf'),
(99, 99, 'KFW1-Revitalisasi10TSKRLKFW(30oktober2018).pdf'),
(100, 100, 'KFW2-KickOfMeetingRevitalisasiKRLKFW(30oktober2018).pdf'),
(101, 101, 'KFW3-NotulenRapat28Januari2019-PemaparanPekerjaanRevitalisasi10TSKRLKFW.pdf'),
(102, 102, 'KFW4-NotulenRapat28Januari2019-RevitalisasiKFW.pdf'),
(103, 103, 'KFW5-ArahanGM,PPC,terkaitRevitalisasiKRL-KFW(29januari2019).pdf'),
(104, 104, 'KFW6-LaporanHasilPekerjaan(30Januari2019).pdf'),
(105, 105, 'KFW7-PembahasanRencanaPenempatanUnitGerbongTerbukaMilikNegara(31Januari2019).pdf'),
(106, 106, 'KFW8-AttendanceList4Februari2019-KonsineeringProyekRevitalisasi10TSKRLKFW.pdf'),
(107, 107, 'KFW9-RisalahRapatKoordinasiTgl2&7Februari2019BreakDownActivity.pdf'),
(108, 108, 'KFW10-PersiapanMateriRapatdenganDJKA(11Februari2019).pdf'),
(109, 109, 'KFW11-TindakLanjutPengecekanKRLKFW(15februari2019).pdf'),
(110, 110, 'KFW12-PembahasanRencanaPenempatan19UnitGerbongMilikNegaradanTrainset@2UnitKereta(19februari2019).pdf'),
(111, 111, 'KFW13-MemantauprosesdissmountingKFWsertamencatatnoKA,Bogie,axle,dangearbox(22-25 Februari 2019).pdf'),
(112, 112, 'KFW14-RapatPembahasanRevitalisasiKRLKFW(25Februari2019).pdf'),
(113, 113, 'KFW15-RapatPenyusunanMetode(11Maret2019).pdf'),
(114, 114, 'KFW16-PembahasanHasilKajianPenurunanDayapadaKeretaRevitalisasiKRLKFWdanJustifikasi(12Maret2019).pdf'),
(115, 115, 'KFW17-Attendancelist13Maret2019-ACKFW.pdf'),
(116, 116, 'KFW18-NotulenRapatEvaluasiProyekRevitalisasiKRLKfW(15Maret2019).pdf'),
(117, 117, 'KFW19-PembahasanTindakLanjutRencanaPenarikanKRLKFW(18Maret2019).pdf'),
(118, 118, 'KFW20-RapatEvaluasiProyekRevitalisasiKRLKFW(22Maret2019).pdf'),
(119, 119, 'KFW21-PermohonanIjinMasukBalaiYasaManggarai(2April2019).pdf'),
(120, 120, 'KFW22-ReviewTaktKFWRevitalisasi(4April2019).pdf'),
(121, 121, 'KFW23-PenyiapanKereta,SettingRangkaianLokodanBandul,TesAnginAirSpringdanBrake,PembuatanBAPengetesanK'),
(122, 122, 'KFW24-RapatTindakLanjutdanEvaluasiRevitalisasiKFW(12April2019).pdf'),
(123, 123, 'KFW25-PembahasanPenarikanKRLKFW(16April2019).pdf'),
(124, 124, 'KFW26-NotulenRapatEvaluasidanTindakLanjutRevitalisasiKRLKFW(22April2019).pdf'),
(125, 125, 'KFW27-PemindahanbogieTS6keTS8KRLKFW(24-26April2019).pdf'),
(126, 126, 'KFW28-PembongkaranKRLKFWTS4,PemindahanBogieTS4-TS8,&PersiapanPenarikanKFWTS8daribyMRIMadiun(24-27Apr'),
(127, 127, 'KFW29-RapatPembahasanScopePekerjaanSPPJProyekKRLKFW(8Mei2019).pdf'),
(128, 128, 'KFW30-RapatTindakLanjutdanEvaluasiRevitalisasiKFW(10Mei2019).pdf'),
(129, 129, 'KFW31-MonitoringPemeriksaandanEvaluasiRevitalisasiKRLKFW(19-21Mei2019).pdf'),
(130, 130, 'KFW32-PembongkaranPanelFRPEndWallRevitalisasiKRLKFW(23Mei2019).pdf'),
(131, 131, 'KFW33-PembahasanPanelElektrikKRLKFW(27Mei2019).pdf'),
(132, 132, 'KFW34-InspectiondanTestingTraksiMotorUntukProyekKRLKFW(28Mei2019).pdf'),
(133, 133, 'KFW35-MenghadiriMeetingBersamaKemenhubMengenaiKRLKFWdanDepoCipinang(19-20Juni2019).pdf'),
(134, 134, 'KFW36-PertemuandenganSatkerPPSPterkaitaddendumKRLKFW(26-27Juni2019).pdf'),
(135, 135, 'KFW37-RapatSatkerPPSPJakarta(10-12Juli2019).pdf'),
(136, 136, 'KFW38-Koordinasi-koordinasipelaksanaanpembongkaranairdryer,kompressor,doorengine,&koordinasidenganBP'),
(137, 137, 'KFW39-PenambahanLingkupKerjadiluarKontrakPekerjaanRevisiKRLKFW(25Juli2019).pdf'),
(138, 138, 'KFW40-MengawasiMengkoordinirKegiatanPembongkaranPenginapanKomponenKRLKFW(25-27Juli2019).pdf'),
(139, 139, 'KFW41-PembentukanTeknisUsulanPenambahanLingkupdiluarKontrakPekerjaanRevisiKRLKFW(30-31Juli2019).pdf'),
(140, 140, 'KFW42-PembuatanBAPenarikan&pengecekanTS5KRLKFW(27-29Agustus2019).pdf'),
(141, 141, 'KFW43-KunjungankePT.ABBdilanjutRapatProgressKFWdenganPJKA(28Agustus2019).pdf'),
(142, 142, 'KFW44-KoordinasiProyekKRLKFWPenarikanKRLKFWTS5Relasi(2-4September2019).pdf'),
(143, 143, 'BG1-Pengiriman15BGBangladeshBatch1(25Januari-7Februari2019).pdf'),
(144, 144, 'BG2-PengurusanCustomsClearenceProyekBGBangladesh(26-30Januari2019).pdf'),
(145, 145, 'BG3-Commisioning15BGBangladeshBatch1(4-16Februari2019).pdf'),
(146, 146, 'BG4-PengurusanCustomsClearence&EndorseDokumen(19-24Maret2019).pdf'),
(147, 147, 'BG5-Pengiriman18BGKeretaBangladeshBatch2(23Maret-7April2019).pdf'),
(148, 148, 'BG6-Commisioning18BGKeretaBangladeshBatch2(31Maret-16April2019).pdf'),
(149, 149, 'BG7-PengurusanCustomClearance&EndorseDokumenProyekBGKeretaBangladeshBatch3(9-15Mei2019).pdf'),
(150, 150, 'BG8-Pengiriman17KeretaBangladeshBatch3(12-28Mei2019).pdf'),
(151, 151, 'BG9-Commisioning17BGKeretaBangladeshBatch3(19Mei-2Juni2019).pdf'),
(152, 152, 'MG1-MenjemputTamuBangladeshRailway(22Februari2019).pdf'),
(153, 153, 'MG2-MenjemputTamuBangladeshRailwayUntukAgendaStudyTour(28Maret2019).pdf'),
(154, 154, 'MG3-siteVisit,BioToiletBersamaTimStudyTourBangladeshRailway(31Maret-1April2019).pdf'),
(155, 155, 'MG4-KoordinasiPengirimanKeretaMGBangladesh(7-10Juli2019).pdf'),
(156, 156, 'MG5-MG5-RIS-KoordinasiKebutuhanSpring200MGBangladesh-0001.pdf'),
(157, 157, 'MG6-PengirimanKeretaMGBatch1(26cars)(28Juli-11Agustus2019).pdf'),
(158, 158, 'MG7-CommisioningKeretaMGBatch1(26 cars)(1-15Agustus2019).pdf'),
(159, 159, 'MG8-CommisioningKeretaMGBangladeshBatch1(lanjutan)(2-11September2019).pdf'),
(160, 160, 'KRDEBias-2019.pdf'),
(183, 160, 'kep_rektor_nomor_5056_tahun_2019_tentang_penerima_beasiswa_peningkatan_prestasi_akademik_tahun_angga'),
(187, 166, 'IMG-20191016-WA0001.jpg'),
(188, 166, 'IMG-20191016-WA0000.jpg'),
(203, 1, 'file.png'),
(205, 161, 'DataTables example - File export.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `tbcategory`
--

CREATE TABLE `tbcategory` (
  `categoriid` int(11) NOT NULL,
  `namakategori` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbcategory`
--

INSERT INTO `tbcategory` (`categoriid`, `namakategori`) VALUES
(1, 'meeting'),
(2, 'training'),
(3, 'inspection'),
(4, 'others'),
(5, 'memo');

-- --------------------------------------------------------

--
-- Table structure for table `tbisu`
--

CREATE TABLE `tbisu` (
  `isuid` int(11) NOT NULL,
  `namaisu` varchar(50) NOT NULL,
  `permasalahan` text NOT NULL,
  `tindaklanjut` varchar(100) NOT NULL,
  `level` varchar(50) NOT NULL,
  `status` varchar(10) NOT NULL,
  `projectid_fk` int(11) NOT NULL,
  `pic` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbisu`
--

INSERT INTO `tbisu` (`isuid`, `namaisu`, `permasalahan`, `tindaklanjut`, `level`, `status`, `projectid_fk`, `pic`) VALUES
(7, 'HV BOX IP 65', 'untuk TS 5-10  barang blm dikirim ke InKa', 'Inspeksi vendor pemasok IMS', 'major', 'open', 8, 10),
(13, 'isu', 'isu1', 'isu2', 'major', 'open', 12, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbproject`
--

CREATE TABLE `tbproject` (
  `projectid` int(11) NOT NULL,
  `namaproject` varchar(100) NOT NULL,
  `pic` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbproject`
--

INSERT INTO `tbproject` (`projectid`, `namaproject`, `pic`) VALUES
(8, 'LRT Jabodebek', 1),
(9, 'KRL KFW', 1),
(10, '50 BG', 1),
(11, '200 MG', 1),
(12, '4 TS KRDE BIAS', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbrecord`
--

CREATE TABLE `tbrecord` (
  `recordid` int(11) NOT NULL,
  `kegiatan` varchar(100) NOT NULL,
  `tglstart` varchar(50) NOT NULL,
  `tglend` varchar(50) NOT NULL,
  `pembahasan` text NOT NULL,
  `tindaklanjut` varchar(100) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `pic` int(11) NOT NULL,
  `tanggalcreate` varchar(50) NOT NULL,
  `kategori` int(11) NOT NULL,
  `projectid_fk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbrecord`
--

INSERT INTO `tbrecord` (`recordid`, `kegiatan`, `tglstart`, `tglend`, `pembahasan`, `tindaklanjut`, `keterangan`, `pic`, `tanggalcreate`, `kategori`, `projectid_fk`) VALUES
(1, 'Rapat Koordinasi progress pekerjaan dan persiapan pembuatan final report proyek LRT Jabodebek ke KEM', '2019-01-04', '2019-01-04', 'Meeting bersama BPPT untuk melakukan rakor progress pekerjaan dan persiapan pembuatan final report proyek LRT Jabodebek ke Kemenkomar', '', 'Materi utama:\n1. RAMS Analysis\n2. Hasil audit teknologi \n3. Crashworhiness\n4. Persiapan uji fatique ', 1, '2019-09-16', 1, 8),
(2, 'Meeting percepatan pembangunan LRT Jabodebek : \n1. Justifikasi enggeseran depo ke cibubur, \n2. Dampa', '2019-01-07', '2019-01-08', '1. Depo rencana penyelesaian pengadaan lahan dengan LMA dan PPK sofker pembangunan LRT Jabodebek. \n2. Akan dilakukan pembahasan lebih lanjut terkait 1 DC, setiap subcont harap melepaskan potensi nilai rupiah karena pengaruh keterlambatan pembayaran, \n3. Design detail tempory depo direncanakan dibangun setelah stasiun harjamukti cibubur.', '', '1. Depo bekasi timur rencana penyelesaian pengadaan bahan (maret 2019) dan LMA dan PPK sofker pemban', 1, '2019-09-16', 1, 8),
(3, 'FAI traction motor untuk proyek LRT Jabodebek', '2019-01-07', '2019-01-09', '1. Diskusi internal dengan CAF terkait rencana FAI\n2. Verivikasi & pemeriksaan dokumen traksi motor dari ABB SHANGHAI\n3. Verivikasi \"WI\" terhadap proses dan pengujian performansi traksi motor\n4. closing meeting', '', 'FAI report terlampir', 1, '2019-09-16', 1, 8),
(4, 'Rapat Pembahasan Schedule Produksi Sarana LRT Jabodebek', '2019-01-09', '2019-01-10', '', '', 'Notulen meeting terlampir', 1, '2019-09-16', 1, 8),
(5, 'Permohonan Penyeseuaian  Interior dan Dimensi Kursi LRT Jabodebek', '2019-01-15', '2019-01-15', '', '', 'Surat Terlampir', 1, '2019-09-16', 1, 8),
(6, '1. Paparan hasil studi pendampingan Light Rail Transit (LRT) Jabodebek, \n2. Paparan hasil studi kela', '2019-01-15', '2019-01-16', '1. Diskusi dengan Bpk Dirut dan dirtekom terkait LRT Jabodebek dan diskusi terkait High Level meeting tgl 16 jan 2019\n2. Menghadiri high level meeting kegiatan perkeretaapian BPPT', '', 'Pada LRT Jabodebek perlu adanya pengujian teknis terkait:\n1. Pengujian untuk penangkal petir pada ro', 1, '2019-09-16', 1, 8),
(7, 'Pemantauan Progress Produksi Sarana LRT Jabodebek', '2019-01-16', '2019-01-18', '', '', 'Notulen meeting terlampir', 1, '2019-09-16', 3, 8),
(8, 'Hasil Kegiatan Perkeretaapian BPPT 2018', '2019-01-16', '2019-01-16', '', '', 'Notulen meeting terlampir', 1, '2019-09-16', 1, 8),
(9, 'Rapat FGD Kegiatan Perkeretaapian', '2019-01-16', '2019-01-16', '', '', 'Di JW.Mariot Surabaya', 1, '2019-09-16', 1, 8),
(10, 'MRB Carbody M-011 LRT Jabodebek', '2019-01-17', '2019-01-17', '', '', '* Notulen meeting terlampir\n* Ruang Rapat QC', 1, '2019-09-16', 1, 8),
(11, 'Evaluasi Carbody LRT Jabodebek', '2019-01-17', '2019-01-17', '', '', '* Notulen meeting terlampir\n* Ruang Rapat QC', 1, '2019-09-16', 1, 8),
(12, 'Informasi Jadwal Kedatangan Sub Assy Carbody LRT Jabodebek', '2019-01-22', '2019-01-22', '', '', 'MOM antara PT. INKA dengan Jiangsu Tedrail Co.Ltd tanggal 7-9 Januari 2019 Terlampir', 1, '2019-09-16', 4, 8),
(13, 'Koordinasi teknis sarana dan progress sarana LRT Jabodebek', '2019-01-22', '2019-01-24', '1. Koordinasi teknis terkait penjelasan deskripsi sistem Public Address& Passenger Information dari Play system sarana LRT Jabodebek. (Blok diagram funetion speci fication)\n2. Pemaparan progress fisik kedatangan komponen dan penyelesaian desain software & configuration network.\n3. Sinkronisasi radio telekomunikasi sistem dengan PA & DIDS, Rolling Stock, terkait kemungkinan kedua sistem tersebut koneksi secara jaringan \n4. Penyampaian progress fisik sarana LRT Jabodebek', '', '1. PA dan PIDS konfigurasi kerja sistemnya berdasar internet protokol dan berbasis local area networ', 1, '2019-09-16', 1, 8),
(14, 'Pengawasan Progress LRT Jabodebek', '2019-01-23', '2019-01-23', '', '', 'Ruang Rapat', 1, '2019-09-16', 1, 8),
(15, 'Evaluasi Carbody Proyek LRT Jabodebek', '2019-01-23', '2019-01-23', '', '', 'Notulen dan memo terlampir', 1, '2019-09-16', 3, 8),
(16, 'Rapat terkait LRT Jabodebek', '2019-01-23', '2019-01-24', '1. Rapat pembahasan keterlambatan pembangunan LRT Jabodebek\n2. Menghadiri undangan meeting kemenkomar jakarta terkait penyampaian progress fisik sarana LRT Jabodebek ', '', 'Hasil Rapat (23 Januari 19)\n1. Laporan Progress Prasarana \n2. Akan dilakukan evaluasi lagi terkait p', 1, '2019-09-16', 1, 8),
(17, 'FAI komponen Air seal projek LRT Jabodebek', '2019-01-23', '2019-01-27', '1. Perjalanan Madiun-Jakarta\n2. Perjalanan Jakarta-Paris\n3. Perjalanan Paris monfbrisen\n4. Factory visit dan FAI Air Seal\n5. Perjalanan Monfbrisen-Paris\n6. Perjalanan Paris-Jakarta\n7. Perjalanan Jakarta-Madiun.', '', '1. Rranggajati, Sriwijaya Air\n2. Technetics Factory, Monfbrisen France\n3. KA. Singosari', 1, '2019-09-16', 4, 8),
(18, 'Pengawasan Progress LRT Jabodebek', '2019-01-24', '2019-01-24', '', '', 'Ruang Rapat Tengah QC', 1, '2019-09-16', 1, 8),
(19, 'Meeting dengan KAI terkait LRT Jabodebek', '2019-01-29', '2019-01-29', 'Meeting dengan KAI terkait LRT Jabodebek membahas IDC', '', 'Hasil Meeting: garis besar cara perhitungan IDC sudah disepakati. INKA diminta membuat detail IDC (t', 1, '2019-09-16', 1, 8),
(20, 'Rapat koordinasi percepatan penyelesaian proyek LRT', '2019-01-31', '2019-01-31', 'Rapat koordinasi percepatan pembangunan dan penyelesaian proyek Infrastruktur (Prasarana, Fasop, Dipo)\nProyek LRT Jabodebek oleh menteri Koor. Maritim , Menteri Perhubungan', '', '1. Pembahasan rencana pembebasan 191 bidang dimana lahan untuk Dipo sarana LRT serta OCC (operating ', 1, '2019-09-16', 1, 8),
(21, 'Pembahasan Denda CAF Terkait LRT Jabodebek', '2019-02-06', '2019-02-06', '', '', 'Ruang Rapat GM Keproyekan', 1, '2019-09-16', 1, 8),
(22, 'Pembahasan Revisi Master Schedule', '2019-02-07', '2019-02-07', '', '', '* Pembahasan / Notulen terlampir\n* Ruang Rapat GM Keproyekan', 1, '2019-09-16', 1, 8),
(23, 'Meeting dengan Deputy Transportasi Pemprov Jakarta; Meeting dengan KAI, PWC, dan staff Kemenkomar', '2019-02-14', '2019-02-15', '1. Makan malam dengan Deputy Transportasi Pemprov Jakarta\n2. Meeting dengan KAI, PWC, dan Kemenkomar untuk membahas IDC LRT Jabodebek\n3. Menghadiri undangan Kemenkomar agenda rapat koordinasi percepatan pembangunan LRT Jabodebek', '', '1. Membahas rencana presentasi INKA tentang kemampuan INKA memproduksi LRT\n2. Masing-masing pihak me', 1, '2019-09-16', 1, 8),
(24, 'Survey lokasi dan pembahasan rencana kerja Temporary Pit Stop', '2019-02-14', '2019-02-15', '1. Peninjauan lokasi area loading/unbading serta area penerimaan kereta di lintas utama/eleveted rail di area stasiun harjamukti \n2. Rapat dengan PT. KAI LRT Jabodebek unit sarana untuk mendapatkan pola stabing dan keluar masuk kereta dari temporary dipo serta fasilitas / peralatan pendukung dipo\n3. Rapat koordinasi pembangunan temporary dipo (PAB dan konsep desain) antara KAI, INKA. AdhiKarya, Ditjen KA (PPK)', '', '1. Tempat loading/ unloading by multiaxle truck akan dipersiapkan pemodalan/ pengerasan untuk parkin', 1, '2019-09-16', 3, 8),
(25, 'Rapat Pembahasan Temporary Pit Stop', '2019-02-15', '2019-02-15', '', 'Tindak Lanjut Terlampir', 'Ruang Rapat Kantor Divisi LRT Jabodebek', 1, '2019-09-16', 1, 8),
(26, 'Rapat Koodinasi Pembahasan IDC proyek LRT Jabodebek, dan meeting pembebasan lahan untuk Dipo LRT Jab', '2019-02-19', '2019-02-20', '1. Meeting koordinasi untuk persiapan rapat 19 februari 2019 bersama PT. KAI Divisi. LRT Jabodebek.\n2. Rapat koordinasi terkait pembahasan IDC (interest duting construction) penyelesaian proyek LRT Jabodebek yang mundur di bulan operasionalnya karena keterlambatan penyelesaian pembangunan DIPO (Bekasi Timur)\n3. Konsolidasi perubahan FS menyesuaikan kemunduran jadwal operasional\n4. Pembahasan pembebasan lahan untuk Dipo LRT Jabodebek di Bekasi Timur\n5. Pembahasan Penlok Stasiun Dukuh Atas.', '', '1. Recana adendum kontrak sarana LRT Jabodebek akan dilakukan setelah adanya kesepakatan nilai IDC+a', 1, '2019-09-16', 1, 8),
(27, 'Pembahasan Schedule Pembayaran Sarana LRT Jabodebek', '2019-02-25', '2019-02-26', '', 'Tindak Lanjut Terlampir', 'Ruang Rapat PT INKA Madiun', 1, '2019-09-16', 1, 8),
(28, 'Monitoring & Evaluasi Rekomendasi Keselamatan Oleh KNKT', '2019-02-26', '2019-02-26', '', 'Tindak Lanjut Terlampir', 'Aula PT INKA (persero)', 1, '2019-09-16', 3, 8),
(29, 'Meeting terkait LRT Jabodebek', '2019-02-28', '2019-03-01', '1. Meeting dengan KAI terkait LRT Jabodebek\n2. Meeting dengan Kemenkomar untuk pembahasan amandemen kontrak LRT Jabodebek\n3. Meeting dengan KAI terkait LRT Jabodebek', '', '1. Simulasi Capex INKA\n2. Pembahasan Capex PT. Adhi Karya (Persero) Tbk dan PT INKA.', 1, '2019-09-16', 1, 8),
(30, 'Koordinasi Uji Fatigue Bogie Frame LRT Jabodebek', '2019-03-05', '2019-03-05', '', '', '* Pembahasan / Notulen terlampir\n* Ruang Rapat B2TKS', 1, '2019-09-16', 1, 8),
(31, 'Preparation meeting perihal pengujian bogie frame dan pembahasan sistem proteksi petir LRT Jabodebek', '2019-03-05', '2019-03-06', '1. Rapat koordinasi dalam evaluasi testting dan penempatan aktuatori sensor dalam pengujian bogie frame LRT Jabodebek (Uji Fatique)\n2. Koordinasi pembahasaan prosedur uji dan metodologi proses dalam uji konstruksi bogie frame\n3. Pembahasan penggunaan standar uji Vic atau EN\n4. Rapat pembahasan fuction descripiton safety equipmen di sarana LRT Jabodebek\n5. Pembahsan sistem grounting sarana dan sistem proteksi petir dalam sarana ', '', '1. Secara fungsi dan jaminan kekuatan struktur sudah baik, perlu dipersiapkan beberapa sparepart con', 1, '2019-09-16', 1, 8),
(32, 'Rapat Pembahasan Proteksi Petir Sarana dan Prasarana LRT Jabodebek', '2019-03-06', '2019-03-06', '', '', '* Notulen meeting terlampir\n* Ruang Rapat Kantor DIV LRT Jabodebek', 1, '2019-09-16', 1, 8),
(33, 'Pembahasan Teknis Emergency Handle, Obstacle Detection, Derailment Detection, dan Proteksi Petir', '2019-03-06', '2019-03-06', '', 'Tindak Lanjut Terlampir', 'Ruang Rapat LRT Jabodebek', 1, '2019-09-16', 1, 8),
(34, 'Pembahasan EN13749 / Standar Pengujian', '2019-03-06', '2019-03-06', '', '', '* Notulen meeting terlampir\n* Ruang Rapat SAT B2TKS', 1, '2019-09-16', 1, 8),
(35, 'Rapat Membahas NCR Wheelset LRT Jabodebek', '2019-03-11', '2019-03-11', 'Pembahasan Terlampir', '', 'Ruang GM Keproyekan', 1, '2019-09-16', 1, 8),
(36, 'Meeting terkait LRT Sumsel dan pembahasan ITP LRT Jabodebek', '2019-03-11', '2019-03-13', '1. Pembahasan data train parameter dari sarana TS 1  sid TS 8 LRT palembang yang di submit INKA dan KAI ke PPK satker LRT \n2. Pembahasan jadwal pengujian dinamik ETCS signalling per TS dan uji Integrasi ETCS signalling sarana, prasarana\n3. Pembahasan keterkaitan Kru KA dan sarana dalam uji ETCS signalling\n4. pembahasan kinematic envelope sarana LRT Jabodebek', '', '1. Adanya deviasi antara data desain dan data real saat dilakukan verifikasi dikarenakan ada range t', 1, '2019-09-16', 1, 8),
(37, 'Courtesy meeting dengan Mr. Arkaitz; meeting dengan Deputy of Transformation', '2019-03-13', '2019-03-14', '1. Courtesy meeting dengan Mr. Arkaitz\n2. Meeting dengan Deputy Gubernur Transportasi ', '', '1. Mengenai kerjasama INKA dan CAF\n2. Mengenai LRT DKI Jakarta', 1, '2019-09-16', 1, 8),
(38, 'Pembahasan Uji Fatigue Bogie Frame LRT Jabodebek', '2019-03-19', '2019-03-19', 'Pembahasan Terlampir', '', '', 1, '2019-09-16', 1, 8),
(39, 'meeting dengan Rotem, meeting dengan tim LRT Jabodebek, menghadiri pameran railtech, meeting dengan ', '2019-03-19', '2019-03-21', '1. Meeting dengan Rotem\n2. Meeting pembahasan amandemen kontrak LRT Jabodebek dengan KAI dan Kemenkomaritim\n3. Lunch meeting dengan Deputy Transportasi DKI tentang proyek LRT Jabodebek\n4. Lunch meeting KRNA Korea\n5. Meeting dengan Dir. SKF Asia Pasifik', '', '5. Mengenai rencana SKF Investasi di Indonesia', 1, '2019-09-16', 1, 8),
(40, 'Meeting dengan LRT Jabodebek dan menghadiri pameran railway dan transportasi', '2019-03-20', '2019-03-21', '1. Pertemuan dengan tim LRT jabodebek di gedung JRC \n2. Pameran Railway Transportasi di JIEX Kemayoran JKT', '', '1. Membahas adendum terim of payment, paparan versi KAI (ditolak oleh INKA), versi INKA : 10-45-35-1', 1, '2019-09-16', 1, 8),
(41, 'Koordinasi Pembahasan Junction Box Inter Car (on roof & Under Frame)   ', '2019-03-22', '2019-03-22', 'Pembahasan Terlampir', 'Tindak Lanjut Terlampir', 'Ruang GM Teknologi', 1, '2019-09-16', 1, 8),
(42, 'Pembahasan Progress LRT Jabodebek / Review Persiapan Bogie Mounting dan Pengujian TS1', '2019-03-25', '2019-03-25', 'Pembahasan Terlampir', 'Tindak Lanjut Terlampir', 'Ruang GM Keproyekan', 1, '2019-09-16', 1, 8),
(43, 'Menghadiri undangan meeting KAI dan meeting dengan Deputy Transportasi', '2019-03-25', '2019-03-25', '1. Menghadiri undangan rapat pembahasan adendum kontrak pengadaan kereta LRT Jabodebek\n2. Meeting dengan Deputy Transportasi, pembangunan Jaya, dan Kjell Hagland', '', '1. Undangan terlampir\n2. Mengenai proyek LRT DKI Jakarta dan Penjajakan Pendanaan dari Swedia', 1, '2019-09-16', 1, 8),
(44, 'Rapat teknis dengan PT. KAI dan training leadership', '2019-03-25', '2019-03-28', '1. Training leadership winning strategy-leaders talk inaguration way\n2. Rapat koordinasi teknis terkait silabus diklat sarana LRT Jabodebek pada waktu training, pembahasan materi, tempat, peserta training dengan tim diklat/SDM PT. KAI\n3. Rapat koordinasi teknis terkait pengujian statis OBCU signalling di rolling stock, dengan PT. KAI. PT Len', '', '1. Innovation harus berjalan seiring berkembangnya/ bertumbuhnya perusahaan, baik itu teknologi, pro', 1, '2019-09-16', 2, 8),
(45, 'Meeting dengan KAI , Adhi Karya, dan Kemenkomar', '2019-03-27', '2019-03-27', '1. Meeting dengan KAI, Adhi Karya, Kemenkomar\n2. Meeting dengan Deputy Transportasi, Pembangunan Jaya dan Kjell Highland', '', '1. Pembagasan cost of fund LRT Jabodebek\n2. Mengenai proyek LRT DKI Jakarta dan penjajakan dari swed', 1, '2019-09-16', 1, 8),
(46, 'Pembahasan Proyek LRT Jabodebek', '2019-03-29', '2019-03-29', '', 'Tindak Lanjut Terlampir', 'Ruang Rapat PT IMS', 1, '2019-09-16', 1, 8),
(47, 'ITP LRT Jabodebek', '2019-04-04', '2019-04-04', '', '', 'Ruang Rapat Teknologi Produksi', 1, '2019-09-16', 1, 8),
(48, 'meeting with mr. Sakaue about rolling stock gauge', '2019-04-04', '2019-04-04', '', '', '', 1, '2019-09-16', 1, 8),
(49, 'meeting tim pendamping JV Stadler dengan Lawyer INKA', '2019-04-08', '2019-04-08', 'Meeting tim pendamping JV Stadler INKA dengan Lawyer', '', 'Keinginan dari INKA sudah tersampaikan, lawyer akan membuat term sheet negosiasi dengan stader ', 1, '2019-09-16', 1, 8),
(50, 'Rapat koordinasi teknis saarana LRT Jabodebek', '2019-04-09', '2019-04-09', '1. Berangkat dari stasiun Madiun naik kereta api Bima pukul 19.38 Wib\n2. Rapat koordinasi teknis dengan direktorat sarana djka PT Adhikarya, PT. KAI, Konsultan Djka (OOG Jopness), PPK, Satker LRT Jabodebek terkait kinematik Envelosarana di area 9 stasiun yang memiliki lengkung dan berpotensi bersentuhan dengan sarana yang berplan( karena konsep deviasi bj dan sarana di area lengkung.\n2. Pembahasan teknis dan administrasi rencana pengujian sarana LRT Jabodebek, untuk test di INKA saat status dan site saat dinamis.', '', '1. Tiba di stasiun gambir hari selasa, 9 april 2019 pukul 06.00 Wib.\n2. Berdasarkan analisa vogel di', 1, '2019-09-16', 1, 8),
(51, 'Koordinasi Teknis Pekerjaan Signalling dan Pekerjaan Pengujian LRT Jabodebek', '2019-04-10', '2019-04-12', '', '', '* Notuelen Rapat Terlampir\n* Aula Bogie PT INKA (persero)', 1, '2019-09-16', 1, 8),
(52, 'Pembahasan terkait stasiun lengkung dan structure gauge antara sarana dan prasarana LRT Jabodebek', '2019-04-11', '2019-04-11', '1.  08.00-13.30 madiun jakarta\n2. 13.30-17.30 meeting\n3. 17.30-23.30 perlanan mendampingi EVP KAI LRT Jabodebek (bpk. Eko Windo) dari solo-madiun.', '', '1. Madiun-solo\n2. Pembahasan terkait stasiun lengkung dan structure gauge antara sarana dan prasaran', 1, '2019-09-16', 1, 8),
(53, 'Rapat komite audit dengan komisaris ; monitoring simulasi proyek LRT Jabodebek', '2019-04-11', '2019-04-11', '1. Rapat komite audit dengan komisaris\n2. Monitoring simulasi proyek LRT Jabodebek', '', 'Materi tentang hasil evaluasi SPI mengenai SK Divisi Baru. Keproyekan', 1, '2019-09-16', 1, 8),
(54, 'Teknis Supply Komponen LRT Jabodebek', '2019-04-18', '2019-04-18', '', 'Tindak Lanjut Terlampir', 'Ruang PM LRT', 1, '2019-09-16', 1, 8),
(55, 'Meeting dengan Rotem dan meeting dengan Deputy Transportasi', '2019-04-24', '2019-04-25', '1. Meeting dengan Rotem\n2. Lunch meeting dengan Deputy Transportasi DKI Jakarta', '', '1. Terkait LRT Jakpro line 1 fase 2\n2. Pembahasan terkait persiapan LRT Jakpro line 1 fase 2 dan per', 1, '2019-09-16', 1, 8),
(56, 'meeting dengan OCG Jopriss terkait LRT Jabodebek', '2019-04-29', '2019-04-30', '1. meeting dengan konsultan OCG Jopriss\n2. meeting dengan PT KAI\n3. meeting dengan Adhi Karya, OCG Jopriss, dan PT KAI', '', '1. gedung menara taspen\n2. stasiun juanda\n3. gedung LRT HOAdhi Karya', 1, '2019-09-16', 1, 8),
(57, 'Rapat koordinasi teknis LRT Jabodebek dan FGO LRT Sumsel', '2019-04-29 dan 2019-05-02', '2019-04-30 dan 2019-05-03', '1. Rapat dengan OCG Jopriss (konsultan PPK sofker LRT Jabodebek DJKA). Membahas kinematic envelope sarana LRT di Staraigh Line (Lurusan) dan Inforface design whell set dinamic play vs 52D checkdril.\n2. Rapat dengan KSP, Waskita Karya PPK Stakhor LRT Sumsel, Len, KAI, membahas keterlambatan cominisionary/ testing peralatan dipo.\n3. FGO operasional dan perawatan serta pembangunan sarana &prasarana LRT Sumsel.', '', '1. INKA telah menyampaikan usulan dengan teknis integrasi dan akan diberlakukan dengan pihak PPK & A', 1, '2019-09-16', 1, 8),
(58, 'Percepatan penyelesaian TS1 LRT Jabodebek ', '2019-05-02', '2019-05-02', '1. Open item di bogie Assy\n2. Open item di tack 4\n3. Open item di tack 5\n4. Open item di tack 6\n5. Consumable', 'Terlampir', 'Ruang Rapat GM Keproyekan', 1, '2019-09-16', 1, 8),
(59, 'Pembahasan detail desain sarana LRT Jabodebek', '2019-05-02', '2019-05-03', '1. Emergency Door Hande\n2. Gap antara running rail dan guide rail\n3. Desain operasional pintu penumpang\n4. PIDS dan PAS', 'Terlampir', 'Direktorat Teknologi PT INKA', 1, '2019-09-16', 1, 8),
(60, 'CAF Visit bersama Potential Customer (Deputy Transportasi DKI Jakarta)', '2019-05-06', '2019-05-12', '1. Perjalanan Jkt-Ams Perjalanan Ams-Bilbao \n2.Visit CAF \n3.Visit CAF Power \n4. Visit Zaragoza\n5. Perjalanan Bilbao-Ams\n6. Perjalanan Ams-Zurich\n7.Enjoying commuter made Stadler\n8. Visit Stadler Factories \n9. Perjalanan Zurich-Ams \n10.Perjalanan Ams-Jakarta', '', '1. 18.40 (6Mei)-06.00(7Mei) \n2. 7 mei pk 09.15-11.20 Beasain\n3. Irura, Gipuzkua, Spain ke \n4.Dipo Tr', 1, '2019-09-16', 1, 8),
(61, 'Penentuan Jarak Gap Checkrail, widening dan PIT pada Stabling', '2019-05-09', '2019-05-09', 'Rapat penentuan jarak GAP checkrail, widening, dan PIT pada stabling', '', 'Notulen meeting terlampir', 1, '2019-09-16', 1, 8),
(62, 'Pembahasan Temporary Pit Stop LRT Jabodebek', '2019-05-10', '2019-05-10', '1. Desain Temporary Pit Stop\n2. Lingkup Pekerjaan di Temporary Stop\n3. Kebutuhan Minimum Prasarana dan Fasilitas Pendukung di Temporary Pit Stop\n4. Lokasi Temporary Pit Stop\n5. Pemasangan 3 rail pada Temporary Pit Stop', 'Terlampir', 'Ruang rapat unit LRT', 1, '2019-09-16', 1, 8),
(63, 'menghadiri rapat terkait LRT Jabodebek', '2019-05-10', '2019-05-10', '1. Menghadiri rapat lanjutan pembahasan temporary PIT\n2. Menghadiri rapat pembahasan persiapan integrasi prasarana & sarana LRT Jabodebek', '', '1. Sudah diikuti, terdapat permintaan data terkait minimum requirement di dipo & rapat lanjutan pada', 1, '2019-09-16', 1, 8),
(64, 'Seminar LRT Sumsel; rapat koordinasi LRT Jabodebek', '2019-05-14', '2019-05-15', '1. Seminar mengenal LRT lebih dekat, dengan civitas akademika/ universitas di Rembang, DJKA, KAI, Waskita, INKA\n2. Peninjauan lokasi stasiun harjamukti dan rencana pembangunan temporary  dipo di sebelah stasiun harjamukti\n3. Rapat koordinasi penentuan construction gage stasiun yang akan dibangun', '', '1. INKA presentasi materi tentang teknologi sarana perkeretaapian LRT Sumsel\n2. Pembangunan tentang ', 1, '2019-09-16', 2, 8),
(65, 'meeting dengan PT. Adhi Karya, OCG Jopriss dan DJKA terkait LRT Jabodebek', '2019-05-21', '2019-05-21', 'diskusi dengan adi karya OCG Jopriss dan DJKA terkait dengan Gap antara kereta dan platform pada stasiun lengkung', '', '', 1, '2019-09-16', 1, 8),
(66, 'Pembahasan Layout Depo LRT Jabodebek', '2019-05-21', '2019-05-22', '1. kapasitas perawatan Heavy Maintenance\n2. Layout Heavy Maintenance Area\n3. Kebutuhan Data (dimensi, loading, powerconsumption)', 'Terlampir', 'Ruang Rapat Divisi Teknologi PT INKA', 1, '2019-09-16', 1, 8),
(67, 'Inspeksi dan Pengawasan Progress Produksi Sarana LRT Jabodebek', '2019-05-23', '2019-05-24', '1. Progres Pekerjaan\n2. Delivery Sarana\n3. Outstanding Issue\n4. Profile Roda\n5. Wheel Rail Interface\n6. Automatic Tight Lock Coupler\n7. Pengujian Statis dinamis  bogie frame\n8. Derailment detection System\n9. Emergency Door System', 'Terlampir', 'Ruang Rapat Divisi Teknologi PT INKA', 1, '2019-09-16', 3, 8),
(68, 'Kick Of Meeting Addendum Pengadaan Sarana 186 Car LRT Jabodebek', '2019-05-24', '2019-05-24', '1. Addendum Perjanjian Pengadaan Sarana LRT Jabodebek\n2. Biaya Peralatan dan biaya operasional Pit Stop sesuai dengan Matriks Lingkup Pekerjaan\n3. Denda Keterlambatan atas penyelesaian barang', 'Terlampir', 'Meeting Room Hotel Amaris Madiun', 1, '2019-09-16', 1, 8),
(69, 'Rapat Pembahasan Temporary Pit Stop', '2019-05-27', '2019-05-27', 'Temporary Pit Stop\n', 'Terlampir', 'Ruang Rapat LRT Jabodebek', 1, '2019-09-16', 1, 8),
(70, 'Meeting bersama Dirkeu dan  64 Dipo LRT Jakpro', '2019-05-28', '2019-05-28', 'Meeting dengan direktur keuangan beserta GM Dipo LRT Jakpro', '', '1. Membahas masalah MSA LRT Jakpro\n2. Secara prinsip manajemen PT LRT Jakarta berminat untuk memberi', 1, '2019-09-16', 1, 8),
(71, 'Mapping persiapan penyelesaian LRT Jabodebek dan DMU Philipina', '2019-06-12', '2019-06-12', '1. Jadwal Penyerahan kereta tidak sesuai kesepakatan awal dengan PT. KAI Potensi mundur dari jadwal\n2. Komponen LRT yang belum aman dan potensi masalah\n3. Jadwal kedatangan komponen/part untuk interior panel, passanger seat & aluminium ekstrusi yang di supply IMS\n4. Untuk uji dinamik awal/TS1 CCD collector akan menggunakan bahan cast iron\n5. Dari hasil trial TS1 dibutuhkan as made drawing mekanik dan elektrik', 'Terlampir', 'Ruang Rapat QC dan MMLH', 1, '2019-09-16', 1, 8),
(72, 'Menghadiri undangan rapat Adhi Karya LRT Jabodebek', '2019-06-25', '2019-06-25', '1. Rapat korrdinasi teknis dengan tim PT.Adhikarya, PT KAI, OCE  Jopriss, PPK Satker LRT DJKA, terkait input kesan urut . Kontruksi sub system terlampir', '', 'INKA sudah memberikan rekomendasi construction gate gap sesuai hasil simulasi DKF dan plossing cr0ss', 1, '2019-09-16', 1, 8),
(73, 'Meeting dengan Rotem & Meeting dengan KAI', '2019-06-26', '2019-06-27', 'A. Meeting dengan Rotem\nB. Meeting dengan KAI :', '', 'A :\n1. Rotem berniat untuk mengajak INKA dalam proyek Jakarta LRT line 1 fase 2\n2. INKA diharapkan d', 1, '2019-09-16', 1, 8),
(74, 'Uji Bogie LRT Jabodebek dan Rapat Koordinasi Integrasi Sarana & Prasarana dan ......', '2019-06-26', '2019-06-27', '1. Rapat Koordinasi terkait pembahasan integrasi prasarana dan sarana LRT Jabodebek dengan PT.KAI, Adhikarya, Systra Int Arupa bina, OCG Jopriss, PPk Satker, LRT DJKA Kemenhub, dan surveyor', '', 'Terkait issue pada pin c dan d akan dibahas -> pertemuan lanjutan untuk detailnya', 1, '2019-09-16', 1, 8),
(75, 'Workshop Depot LRT Jabodebek', '2019-07-01', '2019-07-01', 'Rapat integrasi depot LRT Jabodebek dengan PT. KAI, PT. Adhi Karya, Konsultan KAI, ............ , membahas tentang :\n1. Layout peralatan dan...........\n2. Construction gage di area dipo (pit, walkway, dll)\n3. Dynamic Kinematic Envelope di area dipo saat sarana melintas\n4. pola perawatan sarana mulai harian sampai tahunan', '', '* PT. Adhi Karya dan PT. KAI memohon bantuan pada INKA untuk melakukan analisa dan simulasi ke pada ', 1, '2019-09-16', 2, 8),
(76, 'Koordinasi Percepatan Pembangunan LRT Jabodebek', '2019-07-02', '2019-07-04', 'A. Koordinasi Percepatan Pembangunan LRT Jabodebek\nB. Rapat dengan Kemenkomar\nC. Diskusi PMO dengan Driektur PT Sinergi Daya Mitra\nD. Rapat dengan Divre 3 Sumsel', '', 'A. Meeting dengan Direktur Keuangan & Direktur Teknik KC 1, Diskusi tentang peluang pengadaan, INKa ', 1, '2019-09-16', 1, 8),
(77, 'Rencana Stabling Sarana LRT Jabodebek', '2019-07-02', '2019-07-02', 'Rapat koordinasi dan konsolidasi dengan GM LRT Jakarta beserta tim teknis pembangunan depo LRT Jakarta (Wika) terkait kemungkinan stabling sarana LRT Jabodebek, dimana stabling depo LRT ...... di lantai 2. kemungkinannya terdapat 12 line, 1 line bisa menampung 2 TS LRT Jabodebek.\nKendalanya : terdapat lengkung kecil 40 mm saat menuju dipo, terdapat DKE sarana yang intringement dengan construction gage dipo', '', 'PT. LRT Jakarta, PT. LRT ....., Wika .... memberikan drawing track alignment depo beserta area stabl', 1, '2019-09-16', 1, 8),
(78, 'Pengujian Kekuatan Struktur Bogie Frame Sarana LRT Jabodebek', '2019-07-03', '2019-07-04', '* Pengujian status exeptional load bogie frame sarana LRT Jabodebek\n* Witness pelaksanaan uji sebagai salah satu syarat sertifikasi sarana untuk uji rancang bangun & kontruksi\n* Pelaksanaan disaksikan oleh tim penguji PT. KAI, dan balai pengujian perkeretaapian, serta kasubdit kelainan & pengembangan barang DJKA\n* Mengantar tamu dari PT. KAI dan tim balai pengujian perkeretaapian DJKA ke Jakarta', '', '* Hasil uji statis dilakukan analisa oleh tim B2TKS dan merekomendasikan laporan analisa disampaikan', 1, '2019-09-16', 3, 8),
(79, 'Pembahasan prasarana LRT test run sarana LRT & pengujian dinamik kekuatan struktur bogie frame LRT J', '2019-07-09', '2019-07-12', '* Rapat koordinasi pembahasan penggunaan prasarana LRT untuk digunakan sebagai objek infrastruktur dalam uji dinamik sarana LRT Jabodebek dengan PT. KAI, PT. Adhi Karya, PPK LRT Jabodebek, Balai pengujian KA. Membahas beberapa poin diantaranya, timeline pegujian, syarat sertifikasi sarana & prasarana serta PKS (perjanjian kerja sama) antara INKA, PPK, KAI, Adhi untuk serah guna sementara saat uji integrasi\n* Pengujian dinamik konstruksi bogie frame LRT Jabodebek. start eyele uji ..... untuk 6 juta eyele.\n* Rapat koordinasi pembangunan dipo LRT Jabodebek\n* Pembahasan milestone pengujian sarana & prasarana ...... LRT Jabodebek', '', '* INKA submit data KE, DKE, cross section R sarana di desain constrution gate dipo\n* INKA submit ren', 1, '2019-09-16', 1, 8),
(80, 'Rapat Addendum LRT Jabodebek', '2019-07-16', '2019-07-16', '1. Delievery\n2. Keterlambatan Pengiriman Barang\n3. Dampak Biaya Perubahan Waktu Penyelesaian Barang\n4. Pengujian', '1.1 PT INKA mengusulkan delievery sarana sebagai berikut:', '', 1, '2019-09-16', 1, 8),
(81, 'rapat Div LRT Jabodebek', '2019-07-23', '2019-07-23', '1. Rapat koordinasi teknis depo perihal finalisasi kinematic  ..... pada area depo dan layout heavy & light maintenance building depo\n* INKA telah memberikan data .................. dan DKE sarana LRT Jabodebek di area depo khususnya area transisi / peralihan antara track lurus dan lengkung\n* INKA memberikan inputan untuk penambahan jarak 5 meter area lengkung peralihan di depo untuk mangakomodasi maximum lateral movement sarana LRT\n2. Rapat pembahasan test & ......... GOA 3\n* PT. INKA memberikan & menyampaiakan data rencana detail test activity sarana\n* PT. Adhi Karya belum bisa menyampaikan test ..... maupun durasi masing-masing item test pada Railway System Test', '', '', 1, '2019-09-16', 1, 8),
(82, 'FAI box-box LRT Jabodebek ', '2019-07-22', '2019-07-22', 'Terlampir', '', 'Terlampir', 1, '2019-09-16', 1, 8),
(83, 'Rapat Addendum LRT Jabodebek & Meeting dengan Kemenkamar', '2019-07-23', '2019-07-25', '1. Pembahasan addendum pengadaan sarana 186 Car Light Rail Transit Jabodebek\n2. Meeting dengan PMU\n3. Menghadiri meeting dengan Kemenkamar - pembahasan percepatan pembangunan LRT Jabodebek', '2.1 Sinkronisasi \"need\" INKA & proposal Sinergi Daya Mitra\n2.2 Hari Kamis, 1-8-2019 akan mempresenta', '', 1, '2019-09-16', 1, 8),
(84, 'Pembahasan Power Supply LRT Jabodebek & PKS Test Run Sarana LRT', '2019-07-25', '2019-07-26', '1. Workshop integrasi power supply system dengan ........ terkait dengan sistem ....... sub station / gardu listrik terkait kapastas daya yang dibutuhkan selama akselarasi, deselrasi, dan stabil jalan di kecepatan tertentu\n2. Pembahasan supply power system telekomunikasi, turn out, wesel, PSD\n3. Pembahasan progress pembebasan lahan depo sarana LRT Jabodebek\n4. Pembahasan milestone perjalanan / derasional LRT Jabodebek\n5. Pembahasan isu rolling stock kesesuaian dengan KP 765\n6. Terkait mechanical coupler sarana LRT Jabodebek yang menggunakan tipe automative tight lock harus disampaikan ke KAI / Kemenkamar\n7. Cek kondisi tempat ....... sarana, serta uji coba crane (hasil baik)\n8. Pembahasan kesepakatan antara KAI, INKA, PPK, Adhi Karya terkait rencana uji sarana prasarana', '8. INKA - Adhi Karya diminta bahas detail terkait tugas / tanggung jawab (1 Agustus 2019)', '', 1, '2019-09-16', 1, 8),
(85, 'Pembahasan Status Pengujian Dinamis Bogie LRT ', '2019-07-26', '2019-07-26', '', '', 'Terlampir', 1, '2019-09-16', 1, 8),
(86, 'Permasalahan Uji fatigue Bogie frame LRT Jabodebek', '2019-07-30', '2019-07-30', 'Terlampir', '', 'Terlampir', 1, '2019-09-16', 1, 8),
(87, 'Pembahasan box IP 66 LRT Jabodebek', '2019-08-02', '2019-08-02', '1. Terkait baut menggunakan Long cap nut\n2. Pemasangan marking box menggunakan sika bounding\n3. Inspeksi terhadap audit vendor IMS terkait mutu & kapasitas akan dilakukan oleh INKA & IMS', '', '', 1, '2019-09-16', 1, 8),
(88, 'Site visit Proyek LRT Jabodebek', '2019-08-05', '2019-08-07', '1. Site Visit / Kunjungan lapangan untuk monitoring progress infrastruktur LRT Jabodebek di stasiun TMII, stasium Jati Mulya, Dipo LRT Bekasi Timur\n2. Presentasi progress fisik sarana LRT Jabodebek\n3. Rapat pembahasan teknis rencana FAT sarana, pembahasan tindak lanjut uji fatigue bogie frame sarana LRT Jabodebek\n4. Pembahasan run simulation sarana LRT Jabodebek', '2.1 Pendetailan item pengujian sarana dalam kegiatan FAT\n2.2 Membuat timeline pegujian ulang fatigue', '1. Dengan tim yang terlibat antara lain, KAI, Adhi Karya, INKA, .............\n3. INKA ....... Run cu', 1, '2019-09-16', 1, 8),
(89, 'Persiapan FAT dengan KAI', '2019-08-06', '2019-08-07', '1. Koordinasi terkait persiapan FAT dengan KAI\n2. Meeting dengan direktur proyek LRT Jakarta Fase 1 - Jak pro', '1.1 INKA akan segera membuat protokol FAT untuk direview bersama dengan KAI\n1.2 FAT tetap dilakukan ', '2. Mambahas Stabling LRT di Depo LRT Jakarta', 1, '2019-09-16', 1, 8),
(90, 'Pembahasan Biaya Penyambungan Listrik saat Uji Sarana LRT Jabodebek & FGD Sinkronisasi LRT Jabodebek', '2019-08-12', '2019-08-16', '1. Pembahasan pembiayaan dalam penyambungan listrik dan pemakaian listrik di line 1 main track antara stasiun Harjomuksi - stasiun Ciracas saat uji dinamik sarana LRT Jabodebek (type test) dan (routine test)\n2. Pembahasan / focus group discussion sinkronisasi timeline operasional LRT Jabodebek. Melakukan pembahasan skema delievery sarana dan time score kesiapan infrastruktur\n3. Pembahasan dan pendetailan test protocol dan ITP sarana PT. INKA dalam pelaksanaan Factory acceptance Test', '1. PT. INKA menyatakan bahwa menanggung biaya pemasaran kebutuhan listrik untuk keperluan uji dinami', '', 1, '2019-09-16', 1, 8),
(91, 'FGD Sinkronisasi Penyelenggaraan LRT Jabodebek', '2019-08-14', '2019-08-15', '', '', '', 1, '2019-09-16', 1, 8),
(92, 'Rapat pembahasan Factory Acceptance test (FAT) sarana LRT Jabodebek.', '2019-08-16', '2019-08-16', '1. Paparan PT.INKA\n2. Paparan PT.KAI\n3. Standar Pengujian Factory Acceptance test (FAT)\n4. type test', 'Terlampir', 'Ruang rapat unit RT kantor pusat Bandung', 1, '2019-09-16', 1, 8),
(93, 'MRB Corner Air Seal dan Upper Seat', '2019-08-19', '2019-08-19', '1. Spesifikasi material dalam approval drawing adalah AI 380 sedangkan barang yang dikirim adalah ADC 12,\n2. Diketemukan beberapa kondisi barang yang pecah dan kusam.\n3. Diketemukan barang dengan kondisi adanya indiksi adanya indikasi crack setelah dilakukan pengujian penetrant test.', 'Terlampir', '', 1, '2019-09-16', 1, 8),
(94, 'Pendetailan Timeline LRT Jabodebek', '2019-08-19', '2019-08-20', 'A. Pembahasan Jadwal pengujian railway sistem\n1. Jalan bangunan di setiap line\n2. Pengujian TPS 3 / substation gardu\n3. Pengujian high voltage network dan third rail\n4. Pengujian dinamic rolling stock\nB. Sinkronisasi dengan uji integrasi\nC. Pengecekan kinematic envelope sarana, kesesuaian dengan construction gate prasarana\nD. Pengecekan .... area di stabling entry. dipo lantai 1 dan lantai 2', '', '', 1, '2019-09-16', 1, 8),
(95, 'Survey Ke Depo LRT Jakarta', '2019-08-20', '2019-08-20', 'Survey lokasi depo dan pemaparan hasil simulasi awal interface sarana dengan prasarana', '', 'Dilakukan survey serta pengukuran konstruksi dipo yang berpotensi bersinggungan dengan sarana', 1, '2019-09-16', 3, 8),
(96, 'MRB CCD LRT Jabodebek ', '2019-08-21', '2019-08-21', '1. Respon dari vendor terhadap NCR \n', 'Terlampir', 'Ruang Rapat QC lantai 1', 1, '2019-09-16', 1, 8),
(97, 'Pembahasan Terkait LRT Jabodebek Dengan Dirut KAI & Adhi Karya & Dirut INKA', '2019-08-21', '2019-08-21', 'Pembahasan Terkait LRT Jabodebek bersama Dirut KAI & Adhi Karya & Dirut INKA', '1. INKA mengirimkan kereta ke Jakarta berdasarkan schedule yang disepakati 1 TS, 3 TS, 3 TS, dst\n2. ', '', 1, '2019-09-16', 1, 8),
(98, 'Introduction Scope ISA LRT Jabodebek', '2019-08-29', '2019-08-29', 'Pembahasan persiapan kegiatan safety assesment sub system LRT Jabedebek dalam menuju kesiapan operasional maupun dalam waktu dekat pengujian dinamik sarana LRT Jabodebek. (INKA, KAI, Adhi Karya, IC. KAI, Independent Safely Assesor (ISA) Adhi Karya dari TUV Rheinland ', 'Baik INKA dan KAI dan Adhi Karya sepakat melakukan safely assesment terhadap sub system yang terkait', '', 1, '2019-09-16', 1, 8),
(99, 'Revitalisasi 10 TS KRL KFW ', '2018-10-30', '2018-10-30', 'Pembahasan Terlampir', '', 'Ruang Rapat Pemasaran', 1, '2019-09-18', 1, 9),
(100, 'Kick Of Meeting', '2019-01-15', '2019-01-15', 'Pekerjaan Revitalisasi 10 TS KRL KFW', '', 'Ruang Rapat Pemasaran', 1, '2019-09-18', 1, 9),
(101, 'Pemaparan Pekerjaan Revitalisasi 10 TS KRL KFW', '2019-01-28', '2019-01-28', 'Pembahasan Terlampir', '', 'Ruang Rapat Pemasaran', 1, '2019-09-18', 1, 9),
(102, 'Revitalisasi KFW', '2019-01-28', '2019-01-28', '', 'Tindak lanjut Terlampir', 'Ruang Rapat WS AC Geger', 1, '2019-09-18', 1, 9),
(103, 'Arahan GM, PPC, terkait Revitalisasi KRL-KFW', '2019-01-29', '2019-01-29', 'Pembahasan Terlampir', 'Tindak lanjut terlampir', 'Ruang Rapat PPC', 1, '2019-09-18', 1, 9),
(104, 'Laporan Hasil Pekerjaan', '2019-01-30', '2019-01-30', 'Pembahasan Terlampir', '', '', 1, '2019-09-18', 1, 9),
(105, 'Pembahasan rencana penempatan unit gerbong terbuka milik negara', '2019-01-31', '2019-01-31', '', '', 'Ruang Rapat Direktorat Sarana Perkeretaapian Gd. Menara Thamrin Lt.3A ', 1, '2019-09-18', 1, 9),
(106, 'Konsineering Proyek Revitalisasi 10 TS KRL KFW', '2019-02-04', '2019-02-07', '', '', 'Ruang Rapat Hotel Amaris lt 2', 1, '2019-09-18', 1, 9),
(107, 'Menyusun Breakdown Activity (To Do List) Proyek revitalisasi 10 TS KRL Kfw', '2019-02-04', '2019-02-07', 'Pembahasan Terlampir', '', 'Ruang Rapat Hotel Amaris lt 2', 1, '2019-09-18', 1, 9),
(108, 'Persiapan Materi Rapat dengan DJKA', '2019-02-11', '2019-02-11', 'Pembahasan Terlampir', '', 'Ruang Rapat Pemasaran', 1, '2019-09-18', 1, 9),
(109, 'Tindak Lanjut pengecekan KRL KFW', '2019-02-15', '2019-02-15', '1. Kontrak revitalisasi 10 TS KRL KFW\n2. Konsinyering revitalisasi 10 TS KRL KFW di hotel Amaris ', 'Terlampir', 'Workshop PT.INKA ', 1, '2019-09-18', 1, 9),
(110, 'Pembahasan rencana penempatan 19 unit gerbong milik negara dan trainset @2 unit kereta inspeksi SI 3', '2019-02-19', '2019-02-19', 'Pembahasan Terlampir', 'Terlampir', 'Ruang Rapat Direktorat Sarana Perkeretaapian Gd. Menara Thamrin Lt.3A ', 1, '2019-09-18', 1, 9),
(111, 'Memantau proses dissmounting KFW serta mencatat no KA, Bogie, axle, dan gear box', '2019-02-22', '2019-02-25', '1. Diskoper TCI dan MCI\n-Penepatan Pinbar Couper\n-Pelepasan koneksi Afa Ring\n-Pelepasan koneksi flexible hose\n(stand by) persiapan lengrir kereta mei dan MC 2 ke gedung, KCI manggarai\n2. +Pengecekan komponen bogie\n+Pelepasan koneksi WEA Ring\n+Pelepasan komponen mekanik\n-Bolt mono link\n-Bolt Anti roll bar\n-Bolt traksi motor (2)-Bolt treton Pop\n-Bolt Pipa Tal stay\n3. - Bolt Adafter berring\n-Bolt harmonika\n-Bolt safety wire\n-Pin, path road, cabif order, safety wire\n-Yap oli coupcing blok.\n4.- Pelepasan Mono Link\n- Pelepasan anti roll bar\n- pelepasan center pivot\n- pemindahan car body ke bogie temporary\npengawalan langsung kereta pada jalur stabling', '', '1. Langsung ke jalur gepung kci-manggarai\n2. pekerjaan dilaksanakan mulai jam 08.00-22.00 WIB\n3. pel', 1, '2019-09-18', 3, 9),
(112, 'Koordinasi Progress Proyek KRL KFW', '2019-02-25', '2019-02-25', 'Pembahasan Terlampir', 'Terlampir', 'Ruang Rapat Pemasaran', 1, '2019-09-18', 1, 9),
(113, 'Rapat Penyusunan Metode Perbaikan KRL KFW', '2019-03-11', '2019-03-11', '', 'Tindak lanjut Terlampir', 'Ruang Rapat Pemaaran', 1, '2019-09-18', 1, 9),
(114, 'Pembahasan Hasil Kajian penurunan Daya pada kereta Revitalisasi KRL/KFW Dan Justifikasi desain', '2019-03-12', '2019-03-12', '', 'Tindak lanjut Terlampir', 'Ruang Rapat Satket PPSP Lt 3', 1, '2019-09-18', 1, 9),
(115, 'AC KFW', '2019-03-13', '2019-03-13', '', '', 'Ruang Rapat SBU AC', 1, '2019-09-18', 1, 9),
(116, 'Rapat Evaluasi Proyek Revitalisasi KRL KFW', '2019-03-15', '2019-03-15', '', 'Tindak lanjut Terlampir', 'Ruang Rapat Pemaaran', 1, '2019-09-18', 1, 9),
(117, 'Pembahasan Tindak lanjut rencana penarikan KRL KFW', '2019-03-18', '2019-03-18', '', 'Tindak lanjut Terlampir', 'Ruang Rapat Lokomotif Direktorat Sarana Perkeretaapian', 1, '2019-09-18', 1, 9),
(118, 'Rapat Evaluasi Proyek Revitalisasi KRL KFW', '2019-03-22', '2019-03-22', '', 'Tindak lanjut Terlampir', 'Ruang Rapat Pemasaran', 1, '2019-09-18', 1, 9),
(119, 'Permohonan ijin masuk Balai Yasa Manggarai', '2019-04-02', '2019-04-02', '', 'Tindak Lanjut Terlampir', '', 1, '2019-09-18', 4, 9),
(120, 'Review takt KRL Kfw Revitalisasi', '2019-04-04', '2019-04-04', 'Pembahasan Terlampir', '', 'Ruang Rapat Finishing lt 2', 1, '2019-09-18', 1, 9),
(121, 'Penyiapan kereta, setting rangkaian loko dan bandul, tes angin air spring dan brake, pembuatan BA pe', '2019-04-11', '2019-04-13', '1. Pemeriksaan bersama KRL KFW TS 3 sebelum berangkat (INKA, KCI, PJKA)\n2. Pembuatan BA Pemeriksaan bersama\n3. Pengiriman KRL KFW TS 3 dari MRI ke Madiun\n4. Koordinasi bersama INKA, KCI untuk rencana Pemeriksaaan TS 4,6,5,10 Depok ke By MRI', '', '1. Sebelum dilakukan pemeriksaan, dilakukan pengukuran Roda di depo KRL Bukit Duri (INKA-KJI)\n2. Pen', 1, '2019-09-18', 3, 9),
(122, 'Rapat evaluasi dan tindak lanjut KRL KFW', '2019-04-12', '2019-04-12', 'Pembahasan Terlampir', 'Tindak lanjut Terlampir', 'Ruang Rapat Pemasaran', 1, '2019-09-18', 1, 9),
(123, 'Pembahasan penarikan KRL KFW', '2019-04-16', '2019-04-16', 'Pembahasan Terlampir', '', 'Dipo Depok', 1, '2019-09-18', 1, 9),
(124, 'Rapat Evaluasi & Tindak Lanjut Revitalisasi KRL KFW', '2019-04-22', '2019-04-22', '', 'Tindak lanjut Terlampir', 'Ruang Rapat Pemasaran', 1, '2019-09-18', 1, 9),
(125, 'Pemindahan Bogie TS 6 ke Ts 8 KRL KFW', '2019-04-24', '2019-04-26', '1. -Pembongkaran KFW T5 4 (center plust, anti roolbar\n- Koordinasi dengan OHM KCI ((fasilitas, penyiapan workshop, eksekusi)\n2. -lanjut pembongkaran T5 4\n- pasang lembah center plust dan anti roll bar T58 \n- pemindahan bogie M1&M2 T5 4 ke M! &M2 T5 8\n3. lanjut Assy bogie T5 8 utk di kirim ke Madiun\n4. Pemeriksaan keembali roda', '', '1. Bogie M1&M2 T5 4 diganti dengan dummy bogie\n2. -Koordinasi dengan KCI, DAOP I, \n-Pembuatan BA Pem', 1, '2019-09-18', 4, 9),
(126, 'Pembongkaran KRL KFW TS 4, Pemindahan Bogie TS4 - TS 8 & persiapan penarikan KFW TS 8 dari by MRI -M', '2019-04-24', '2019-04-27', '', '', '', 1, '2019-09-18', 4, 9),
(127, 'rapat pembahasan scope pekerjaan SPPJ proyek KRL-KFW', '2019-05-08', '2019-05-08', 'Pembahasan Terlampir', 'Tindak Lanjut Terlampir', 'Ruang Rapat Finishing', 1, '2019-09-18', 1, 9),
(128, 'Rapat tindak lanjut dan evaluasi revitalisasi KRL KFW', '2019-05-10', '2019-05-10', 'Pembahasan Terlampir', 'Tindak Lanjut Terlampir', 'Ruang Rapat Finishing', 1, '2019-09-18', 1, 9),
(129, 'Monitoring Pemeriksaan dan evaluasi revitalisasi 10 trainset KRL KFW', '2019-05-19', '2019-05-21', '', 'Tindak lanjut Terlampir', 'Ruang Rapat Pemasaran', 1, '2019-09-18', 1, 9),
(130, 'Pembongkaran panel FRP End wall revitalisasi KRl KFW', '2019-05-23', '2019-05-23', '', 'Tindak lanjut Terlampir', 'Ruang Rapat Finishing', 1, '2019-09-18', 1, 9),
(131, 'Pembahasan Panel elektrik KRL KFW', '2019-05-27', '2019-05-27', '', 'Tindak lanjut Terlampir', 'Ruang Rapat Teknologi Produksi', 1, '2019-09-18', 1, 9),
(132, 'Inspection dan testing traksi motor untuk proyek KRL KFW', '2019-05-28', '2019-05-28', '1. Perjalaanan Madiun-Surabaya \n2. Perjalanan Stasiun Gubeng-PT. ABB Margomulyo\n3. Tes dan Inspeksi proses Pemeriksaan dan pengujian traksi motor di ABB \n4. Perjalanan PT. ABB Margomulyo-Stasiun Gubeng\n5. Perjalanan surabaya-madiun.', '', '', 1, '2019-09-18', 3, 9),
(133, 'menghadiri meeting bersama kemenhub mengenai KRL KFW dan Depo Cipinang\n', '2019-06-19', '2019-06-20', '1. Pembahasan Penggusuran depo Cipinang\n2. Colour Scheme\n3. Trac KRL Solo-Yogya\nKoordinasi dengan PPK untuk progress KFW\nKooordinasi penambahan anggaran', '', 'Malang', 1, '2019-09-18', 1, 9),
(134, 'Pertemuan dengan Satker PPSP terkait addendum KRL KFW', '2019-06-26', '2019-06-27', '1. Koordinasi persetujuan colour scheme\n2. Koordinasi progress dan tambahan anggaran\n3. Koordinasi penggunaan depo di MRI\n4. Identifikasi bom KFW di MRI', '', '1. Kantor Satker Jakarta\n2. Kantor KCI dan Depo Mri Jakarta', 1, '2019-09-18', 1, 9),
(135, 'Rapat Satker PPSP, Jakarta', '2019-07-10', '2019-07-12', '1. Koordinasi penarikan kereta dan pelepasan komponen \n2. Rapat Bakp. KFW \n3. Pemutahiran Program Mutu\n4. Pembahasan Colour Seteme\n5. Usulan Penambahan anggaran KFW yang belum masuk kontrak.', '', '1. Daop 1&2 MRI\n2. SATKER\n3. SATKER', 1, '2019-09-18', 1, 9),
(136, 'Koordinasi-koordinasi pelaksanaan pembongkaran air dryer, kompressor, door engine & koordinasi denga', '2019-07-24', '2019-07-26', '1. Koordinasi pembongkaran Door engine & compressor\n2. - Koordinasi dengan BPKP untuk penambahan kegiatan diluar kontrak\n- Pengambilan compressor di depo depok \nkoordinasi dengan Satker\n3.-  Pengiriman door engine ke festo\n- pengiriman compressor ke pindad\n- kontrak pengiriman KFL KFW MRI-MN\n4. Memindahkan rubber spring ke kereta TS 5 yang akan dikirim ke INKA\n5. Pengecekan ulang komponen 4 TS yang ada di MRI\n6. Pengembalian alat-alat ke Daop KCI dan MRI', '', '1. Depo KCI MRI Jakarta\n2.BPKP Jakarta\n3. Depo Depok\n4. Jakarta\n5. KCI MRI ke Bekasi dan Bandung\n6. ', 1, '2019-09-18', 1, 9),
(137, 'penambahan lingkup kerja diluar kontrak pekerjaan revisi KRL KFW', '2019-07-25', '2019-07-25', 'konsultasi rencana pendampingan BPKP terkait revisi usulan penagihan biaya revitalisasi proyek kereta KFW', '', 'Kantor pusat BPKP jakarta', 1, '2019-09-18', 1, 9),
(138, 'Mengawasi mengkoordinir kegiatan pembongkaran penginapan komponen KRL/KFW', '2019-07-25', '2019-07-27', '1. mengawasi kegiatan pembongkaran komponen :\n- door engine\n- cylinder pneumatic\n- panel DMV proyek KRL-KFW\n2. Mengkoordinir pengiriman komponen \n- cylinder pneumatic\n- door engine\n- compressor\n- panel Pmv\n3. Koordinasi surat Administrasi dengan pihak KCI', '', '1. Kegiatan dimulai pukul 08.30-16.30 di balaiyasa Manggarai bersama finishing\n2. kegiatan dimulai p', 1, '2019-09-18', 3, 9),
(139, 'Pembentukan teknis usulan penambahan lingkup di luar kontrak pekerjaan revisi KRL/KRW', '2019-07-30', '2019-07-31', '1. Rapat pembahasan kegiatan diluar kontrak\n2. koordinasi dengan satker untuk penawaran harga/biaya penambahan ', '', '1. Direktorat Sarana jakarta\n2. Satker', 1, '2019-09-18', 1, 9),
(140, 'Pembuatan BA Penarikan & pengecekan TS 5 KRl KFW', '2019-08-27', '2019-08-29', '', '', '', 1, '2019-09-18', 1, 9),
(141, 'Kunjungan ke PT. ABB dilanjut rapat progress KFW dengan PJKA', '2019-08-28', '2019-08-28', '1. berangkat kunjungan ke PT. ABB bersama konsultan KFW, untuk mengecek progres perbaikan Traction motor\n2. Rapat progres KRl KFW bersama DJKA dan Bapak Dikomtek', '', '1. Proyek KRL KFW waktu pelaksanaan 13.00-16.00\n2. waktu pelaksanaan 19.00-21.00', 1, '2019-09-18', 3, 9),
(142, 'Koodinasi proyek KRL KFW- Penarikan KRL KFW TS 5 relasi', '2019-09-02', '2019-09-04', '', '', '', 1, '2019-09-18', 1, 9),
(143, 'Pengiriman 15 BG Bangladesh Batch 1', '2019-01-25', '2019-02-07', '', '', '', 1, '2019-09-18', 4, 10),
(144, 'Pengurusan Customs Clearance Proyek BG Bangladesh', '2019-01-26', '2019-01-30', '', '', '', 1, '2019-09-18', 4, 10),
(145, 'Commisioning 15 BG Bangladesh Batch 1', '2019-02-04', '2019-02-16', '', '', '', 1, '2019-09-18', 2, 10),
(146, 'Pengurusan Custom Clearance & Endorse Dokumen', '2019-03-19', '2019-03-24', '', '', '', 1, '2019-09-18', 4, 10),
(147, 'Pengiriman 18 BG Kereta Bangladesh Batch 2', '2019-03-23', '2019-04-07', '', '', '', 1, '2019-09-18', 4, 10),
(148, 'Commisioning 18 BG Kereta Bangladesh Batch 2', '2019-03-31', '2019-04-16', '', '', '', 1, '2019-09-18', 2, 10),
(149, 'Pengurusan Custom Clearance & Endorse Dokumen Proyek BG Kereta Bangladesh Batch 3', '2019-05-09', '2019-05-15', 'Endorse Dokumen, Custom Clearance, Shipment 3 proyek 50 BG Bangladesh', '1. Dokumen dari Sonali Bank dikirim / diteruskan ke Bangladesh Bank untuk dilakukan pengecekan dan s', '', 1, '2019-09-18', 4, 10),
(150, 'Pengiriman 17 BG Kereta Bangladesh Batch 3', '2019-05-12', '2019-05-28', '', '', '', 1, '2019-09-18', 4, 10),
(151, 'Commisioning 17 BG Kereta Bangladesh Batch 3', '2019-05-19', '2019-06-02', '', '', '', 1, '2019-09-18', 2, 10),
(152, 'Menjemput Tamu Bangladesh Railway', '2019-02-22', '2019-02-22', '', '', 'di Surabaya', 1, '2019-09-18', 4, 11),
(153, 'Menjemput Tamu Bangladesh Railway untuk Agenda Study Tour', '2019-03-28', '2019-03-28', '', '', 'di Bandara Juanda, Surabaya', 1, '2019-09-18', 4, 11),
(154, 'Site Visit, Bio Toilet bersama Tim Study Tour Bangladesh Railway', '2019-03-31', '2019-04-01', '1. Mendampingi team Bangladesh Railway Study Tour\n2. Mendampingi team Bangladesh Railway studi banding toilet ramah lingkungan', '', '1. di DIY dan sekitarnya\n2. di Depo Yogyakarta', 1, '2019-09-18', 4, 11),
(155, 'Koordinasi Pengiriman Kereta MG Bangladesh', '2019-07-07', '2019-07-10', '1. Meeting dengan PD, CME Project\n2. Site visit kereta BG', '', '1. Notulen terlampir\n2. di Stasiun Kamlapur Dhaka', 1, '2019-09-18', 1, 11),
(156, 'Koordinasi Kebutuhan Spring 200 MG Bangladesh', '2019-07-17', '2019-07-17', 'Pembahasan Terlampir', 'Tindak lanjut Terlampir', 'Ruang Rapat Logistik 1.6', 1, '2019-09-18', 1, 11),
(157, 'Pengiriman Kereta MG Batch 1 (26 cars)', '2019-07-28', '2019-08-11', '', '', 'ke Bangladesh', 1, '2019-09-18', 4, 11),
(158, 'Commisioning Kereta MG Batch 1 (26 cars)', '2019-08-01', '2019-08-15', '', '', 'ke Bangladesh ', 1, '2019-09-18', 2, 11),
(159, 'Commisioning Kereta MG Bangladesh Batch 1 (Lanjutan)', '2019-09-02', '2019-09-11', '', '', 'ke Bangladesh ', 1, '2019-09-18', 2, 11),
(160, 'Pengadaan 4 TS KRDE Bandara International Adi Soemarmo\r\n', '2019-07-24', '2019-07-24', '', 'Tindak lanjut Terlampir\r\n', 'Ruang Rapat Pemasaran\r\n', 1, '2019-09-18', 1, 12);

-- --------------------------------------------------------

--
-- Table structure for table `tbuser`
--

CREATE TABLE `tbuser` (
  `userid` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jeniskelamin` varchar(5) NOT NULL,
  `jabatan` varchar(25) NOT NULL,
  `project_to_pm` varchar(11) DEFAULT NULL,
  `role_user` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbuser`
--

INSERT INTO `tbuser` (`userid`, `username`, `password`, `nama`, `jeniskelamin`, `jabatan`, `project_to_pm`, `role_user`) VALUES
(1, 'Admin', '0192023a7bbd73250516f069df18b500', 'Admin', 'L', 'staff', '', '001'),
(2, 'panji', 'ad13728f05381dc91185118df071df07', 'Panji Sulaksono', 'L', 'PM LRT JABODEBEK', '8', '003'),
(3, 'bambang', '357c5eaefeda3bf103d525b58d3fef73', 'Bambang Kushendarto', 'L', 'GM', '', '002'),
(10, 'pastiadi', 'acfb7032f2066cb078a3932270fd80a6', 'Emanuel Pastiadi', 'L', 'SM', '', '002'),
(29, 'wely', '3fdeab58e4159271e2d9b653946dcdea', 'Wely Mulyono', '', 'PM KRL KFW', '9', '003');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbactivityplan`
--
ALTER TABLE `tbactivityplan`
  ADD PRIMARY KEY (`activityplanid`);

--
-- Indexes for table `tbattachment`
--
ALTER TABLE `tbattachment`
  ADD PRIMARY KEY (`attachmentid`);

--
-- Indexes for table `tbcategory`
--
ALTER TABLE `tbcategory`
  ADD PRIMARY KEY (`categoriid`);

--
-- Indexes for table `tbisu`
--
ALTER TABLE `tbisu`
  ADD PRIMARY KEY (`isuid`);

--
-- Indexes for table `tbproject`
--
ALTER TABLE `tbproject`
  ADD PRIMARY KEY (`projectid`);

--
-- Indexes for table `tbrecord`
--
ALTER TABLE `tbrecord`
  ADD PRIMARY KEY (`recordid`);

--
-- Indexes for table `tbuser`
--
ALTER TABLE `tbuser`
  ADD PRIMARY KEY (`userid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbactivityplan`
--
ALTER TABLE `tbactivityplan`
  MODIFY `activityplanid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tbattachment`
--
ALTER TABLE `tbattachment`
  MODIFY `attachmentid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=206;

--
-- AUTO_INCREMENT for table `tbcategory`
--
ALTER TABLE `tbcategory`
  MODIFY `categoriid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbisu`
--
ALTER TABLE `tbisu`
  MODIFY `isuid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tbproject`
--
ALTER TABLE `tbproject`
  MODIFY `projectid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbrecord`
--
ALTER TABLE `tbrecord`
  MODIFY `recordid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=163;

--
-- AUTO_INCREMENT for table `tbuser`
--
ALTER TABLE `tbuser`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
